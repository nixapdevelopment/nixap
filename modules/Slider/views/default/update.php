<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Menu\Menu */

$this->title = 'Update Slider: ' . $sliderModel->Name;
$this->params['breadcrumbs'][] = ['label' => 'Sliders', 'url' => ['index']];
$this->params['breadcrumbs'][] = $sliderModel->Name;
?>
<div class="slider-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'sliderModel' => $sliderModel,
        'dataProvider' => $dataProvider,
    ]) ?>

</div>
