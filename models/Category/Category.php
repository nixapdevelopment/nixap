<?php

namespace app\models\Category;

use Yii;
use yii\helpers\Url;

/**
 * This is the model class for table "Category".
 *
 * @property integer $ID
 * @property string $Image
 * @property string $Link
 * @property string $Status
 *
 * @property CategoryLang[] $categoryLangs
 */
class Category extends \yii\db\ActiveRecord
{
    
    const StatusActive = 'Active';
    const StatusInactive = 'Inactive';
    
    public static function getStatusList()
    {
        return [
            self::StatusActive => Yii::t('app', 'Active'),
            self::StatusInactive => Yii::t('app', 'Inactive')
        ];
    }
    
    public static function getStatusLabel($status)
    {
        $statusList = self::getStatusList();
        return $statusList[$status];
    }
    
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'Category';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['Link', 'Status'], 'required'],
            [['Image'], 'file', 'skipOnEmpty' => true],
            [['Link', 'Status'], 'string', 'max' => 20],
            [['Link'], 'unique']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'ID' => 'ID',
            'Image' => Yii::t('app', 'Image'),
            'Status' => Yii::t('app', 'Status'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCategoryLangs()
    {
        return $this->hasMany(\app\models\CategoryLang\CategoryLang::className(), ['CategoryID' => 'ID'])->orderBy('ID');
    }
    
    public function getImage()
    {
        return $this->hasOne(\app\models\Image\Image::className(), ['ID' => 'Image']);
    }
    
    public function getCurrentLang($lang = 'ro')
    {
        return $this->hasOne(\app\models\CategoryLang\CategoryLang::className(), ['CategoryID' => 'ID'])->where(['LangID' => Yii::$app->language]);
    }
    
    public function getImageLink()
    {
        return Url::to('@web/uploads/category/' . $this->image->Thumb);
    }
    
}
