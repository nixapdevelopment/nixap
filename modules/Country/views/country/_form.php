<?php

use yii\bootstrap\Tabs;

/* @var $this yii\web\View */
/* @var $model app\models\Country\Country */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="country-form">
    
    <?= Tabs::widget([
        'items' => [
            [
                'label' => 'Main',
                'content' => $this->render('_country_form', [
                    'model' => $model,
                    'langsModels' => $langsModels
                ]),
            ],
            [
                'label' => 'Regions',
                'content' => $this->render('_regions', [
                    'model' => $model,
                    'countryRegionsDataProvider' => $countryRegionsDataProvider
                ]),
            ],
        ]
    ]); ?>

</div>