<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Page\Page */

$this->title = 'Update Page: ' . $model->currentLang->Title;
$this->params['breadcrumbs'][] = ['label' => 'Pages', 'url' => ['index']];
$this->params['breadcrumbs'][] = $model->currentLang->Title;
?>
<div class="category-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
        'pageLangModels' => $pageLangModels,
    ]) ?>

</div>
