<?php

namespace app\modules\Feedback\components\FeedbackWidget;

use Yii;
use yii\helpers\Html;
use yii\base\Widget;
use app\models\Feedback\Feedback;


class FeedbackWidget extends Widget
{
    
    public $success = false;

    public function run()
    {
        $model = new Feedback();
        $widgetID = $this->getId();
        
        if (Yii::$app->request->isPost && Yii::$app->request->post("widgetID", false) == $widgetID)
        {
            if ($model->load(Yii::$app->request->post()) && $model->validate())
            {
                $model->save();
                
                $files= \yii\web\UploadedFile::getInstancesByName("File");
                
                foreach($files as $file){
                    $name = md5($file->name . microtime()) . "." . $file->extension;
                    
                    if($file->saveAs(Yii::getAlias("@webroot/uploads/feedback/$name"))){
                        $fileModel = new \app\models\FeedbackFile\FeedbackFile([
                            "FeedbackID" => $model->ID,
                            "File" => $name,
                        ]);
                        
                        
                        $fileModel->save();
                    }
                }
                
                Yii::$app->session->setFlash('feedbackSend', true);
            }
        }
        
        return $this->render('index', [
            'model' => $model,
            "widgetID" => $widgetID,
        ]);
    }
    
}