<?php

    use app\modules\Feedback\components\FeedbackWidget\FeedbackWidget;
    use app\modules\Article\components\ArticleImageWidget\ArticleImageWidget;
    use app\modules\Article\components\ArticleFileWidget\ArticleFileWidget;
    use app\modules\Settings\Settings;
    
    $this->title = $article->lang->Title;
    
?>

<div class="wrapper services-page">
        
    <?= $this->render("../layouts/blocks/header_block") ?>
    
    <div class="uk-clearfix about-services">
        <div class="uk-container-center uk-container">
            <div class="uk-grid">
                <div class="uk-width-medium-3-10">
                    <div class="side-bar">
                        <div class="left-sidebar sidebar3">
                            <div class="title">
                                <h4>
                                    <?= $article->lang->Title ?>
                                </h4>
                            </div>
                            <div class="blog-dropdown">
                                <a href="#">
                                    <i class="uk-icon-navicon"></i>
                                </a>
                            </div>
                            <ul>
                                <li class="active">
                                    <a href="#">
                                        Lorem ipsum dolor sit amet
                                    </a>
                                </li>
                                <li>
                                    <a href="#">
                                        Aliquam tincidunt
                                    </a>
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>
                <div class="uk-width-medium-7-10">
                    <div class="description">
                        <div class="title">
                            <h3>
                                <?= $article->lang->Title ?>
                            </h3>
                            <span></span>
                            <p>
                                Morbi in sem quis dui placerat ornare. Pellentesque odio nisi, euismod in, pharetra
                                a, ultricies in, diam. Sed arcu. Cras consequat.
                            </p>
                            <span></span>
                        </div>
                        <div class="more-description">
                            <?= $article->lang->Text ?>
                        </div>
                        <div class="form">
                            
                            <?= FeedbackWidget::widget() ?>
                            
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
            
    <?= $this->render("../layouts/blocks/footer_block") ?>
    
    </div>
