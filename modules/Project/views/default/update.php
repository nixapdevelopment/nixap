<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Project\Project */

$this->title = Yii::t('app', 'Update {modelClass}: ', [
    'modelClass' => 'Project',
]) . $model->Name;
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Projects'), 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->Name, 'url' => ['view', 'id' => $model->ID]];
$this->params['breadcrumbs'][] = Yii::t('app', 'Update');
?>
<div class="project-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
