<?php

namespace app\models\MenuItem;

use Yii;
use app\models\Article\Article;
use app\models\Menu\Menu;
use app\models\MenuItemLang\MenuItemLang;

/**
 * This is the model class for table "MenuItem".
 *
 * @property integer $ID
 * @property integer $MenuID
 * @property integer $ArtcileID
 * @property integer $ParentID
 * @property integer $Position
 *
 * @property Article $article
 * @property Menu $menu
 * @property MenuItemLang[] $langs
 * @property MenuItemLang $lang
 */
class MenuItem extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'MenuItem';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['MenuID', 'ArtcileID'], 'required'],
            [['MenuID', 'ArtcileID', 'ParentID', 'Position'], 'integer'],
            [['ArtcileID'], 'exist', 'skipOnError' => true, 'targetClass' => Article::className(), 'targetAttribute' => ['ArtcileID' => 'ID']],
            [['MenuID'], 'exist', 'skipOnError' => true, 'targetClass' => Menu::className(), 'targetAttribute' => ['MenuID' => 'ID']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'ID' => 'ID',
            'MenuID' => 'Menu ID',
            'ArtcileID' => 'Artcile ID',
            'ParentID' => 'Parent ID',
            'Position' => 'Position',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getArticle()
    {
        return $this->hasOne(Article::className(), ['ID' => 'ArtcileID']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getMenu()
    {
        return $this->hasOne(Menu::className(), ['ID' => 'MenuID']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getLangs()
    {
        return $this->hasMany(MenuItemLang::className(), ['MenuItemID' => 'ID'])->indexBy('LangID');
    }
    
    /**
     * @return \yii\db\ActiveQuery
     */
    public function getLang()
    {
        return $this->hasOne(MenuItemLang::className(), ['MenuItemID' => 'ID'])->where(['LangID' => Yii::$app->language]);
    }
}
