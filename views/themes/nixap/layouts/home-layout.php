<?php

/* @var $this \yii\web\View */
/* @var $content string */

use yii\bootstrap\Html;
use app\assets\NixapFrontAssets;
use app\modules\Menu\components\MenuWidget\MenuWidget;
use yii\helpers\Url;
use app\modules\Settings\Settings;
use app\models\Article\Article;
use app\models\SliderItem\SliderItem;
use yii\caching\TagDependency;

$bundle = NixapFrontAssets::register($this);

$slides = SliderItem::getDb()->cache(function ($db) {
        return SliderItem::find()->with('lang')->where(['SliderID' => 1])->all();
    }, 30, new TagDependency(['tags' => 'slider']));

?>

<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">
<head>
    <meta charset="<?= Yii::$app->charset ?>">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <?= Html::csrfMetaTags() ?>
    <link rel="shortcut icon" href="<?= Url::to('@web/uploads/settings/' . Settings::getByName('favicon')) ?>" type="image/ico">
    <title><?= Html::encode($this->title) ?></title>
    <script>var SITE_URL = '<?= Url::home(true) ?>';</script>

    <!-- Redirect to http://browsehappy.com/ on IE 8-  -->
    <!--[if lte IE 8]>
    <style type="text/css">
        body{display:none!important;}
    </style>
    <meta http-equiv="refresh" content="0; url=http://browsehappy.com/">
    <![endif]-->
    
    <?php $this->head() ?>
</head>
<body>
 
<?php $this->beginBody() ?>
    
    <div class="wrapper home">
            
        <?= $this->render("blocks/header_block") ?>

        <div class="uk-clearfix our-slider">
            <div class="uk-container-center uk-container">
                <div class="slider owl-carousel owl-theme uk-clearfix">

                    <?php if (count($slides) > 0) { ?>
                        <?php foreach ($slides as $slide) { ?>
                    
                        <div class="item">
                            <?= Html::img($slide->imageUrl) ?>
                            <div class="caption">
                                <h1>
                                    <?= $slide->lang->Title ?>
                                </h1>
                                <p>
                                    <?= $slide->lang->Text ?>
                                </p>
                                <a class="btn details" href="<?= $slide->lang->Link ?>">
                                    <?= Yii::t("app", "DETALII") ?>
                                </a>
                            </div>
                        </div>
                    
                        <?php } ?>
                    <?php } ?>

                </div>
            </div>
        </div>

        <?= $this->render("blocks/footer_block") ?>
        
    </div>
    
    <?= $this->render("blocks/start_project_modal") ?> 
    <?= $this->render("blocks/menu_modal") ?>
    
    <?= $this->registerJs("$('.owl-dot > span').after('" . Html::img($bundle->baseUrl . "/images/dots.png")."'');$('.owl-dot > span').before('" . Html::img($bundle->baseUrl . "/images/dots.png")."');")?>
    
<?php $this->endBody() ?>
</body>
</html>
<?php $this->endPage() ?>
