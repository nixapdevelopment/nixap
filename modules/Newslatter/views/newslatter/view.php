<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\Newslatter\Newslatter */

$this->title = $model->ID;
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Newslatters'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="newslatter-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
         <?= Html::a('<i class="fa fa-arrow-left" aria-hidden="true"></i> &nbsp;' . Yii::t('app', 'Back to list'), ['index'], [
            'class' => 'btn btn-info',
        ]) ?>
        &nbsp;&nbsp;
        <?= Html::a('<i class="fa fa-trash"></i> &nbsp;' . Yii::t('app', 'Delete'), ['delete', 'id' => $model->ID], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => Yii::t('app', 'Are you sure you want to delete this item?'),
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'ID',
            'Email:email',
            'Date',
        ],
    ]) ?>

</div>
