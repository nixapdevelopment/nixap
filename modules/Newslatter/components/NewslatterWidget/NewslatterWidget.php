<?php

namespace app\modules\Newslatter\components\NewslatterWidget;

use Yii;
use yii\base\Widget;
use app\models\Newslatter\Newslatter;


class NewslatterWidget extends Widget
{
    
    public $success = false;

    public function run()
    {
        $model = new Newslatter();
        
        if (Yii::$app->request->isPost)
        {
            if ($model->load(Yii::$app->request->post()) && $model->validate())
            {
                $model->save();
                
                Yii::$app->session->setFlash('newslatterSend', true);
            }
        }
        
        return $this->render('index', [
            'model' => $model,
        ]);
    }
    
}