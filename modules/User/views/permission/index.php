<?php 

use yii\bootstrap\Tabs;
use yii\bootstrap\Html;

?>
<h1>User module permissions</h1>
<div>
    <?php 
    echo Tabs::widget([
        'items' => $tabItems
    ]);
    ?>
</div>
<br />
<?= Html::beginForm() ?>
<div class="row">
    <?php foreach ($permissions as $permission) { ?>
    <div class="col-md-3">
        <?= Html::label($permission['label']) ?>
        <?= Html::checkbox('permissions[' . $permission['permission'] . ']', in_array($permission['permission'], $userPermissionsList)) ?>
    </div>
    <?php } ?>
    <?= Html::hiddenInput('module', $currentModule->id) ?>
</div>
<hr />
<?= Html::submitButton('Save', [
    'class' => 'btn btn-success'
]) ?>
<?= Html::endForm() ?>