<?php

namespace app\modules\Newslatter;

/**
 * Newslatter module definition class
 */
class Newslatter extends \yii\base\Module
{
    /**
     * @inheritdoc
     */
    public $controllerNamespace = 'app\modules\Newslatter\controllers';

    /**
     * @inheritdoc
     */
    public function init()
    {
        parent::init();

        // custom initialization code goes here
    }
}
