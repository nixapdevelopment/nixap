/* PROGRES CIRCL */

var bar = new ProgressBar.Circle(box, {
    color: '#3e8cbd',
    // This has to be the same size as the maximum width to
    // prevent clipping
    strokeWidth: 4,
    trailWidth: 1,
    easing: 'easeInOut',
    duration: 5000,
    text: {
        autoStyleContainer: false
    },
    from: { color: '#3e8cbd', width: 1 },
    to: { color: '#3e8cbd', width: 4 },
    // Set default step function for all animate calls
    step: function(state, circle) {
        circle.path.setAttribute('stroke', state.color);
        circle.path.setAttribute('stroke-width', state.width);

        var value = Math.round(circle.value() * 5);
        if (value === 0) {
            circle.setText('');
        } else {
            circle.setText(value);
        }

    }
});


bar.text.style.fontFamily = '"Heavy",  sans-serif';
bar.text.style.fontSize = '80px';

bar.animate(1.0);  // Number from 0.0 to 1.0


/* PROGRES BAR CIRCLE 2 */

var bar = new ProgressBar.Circle(box2, {
    color: '#3e8cbd',
    // This has to be the same size as the maximum width to
    // prevent clipping
    strokeWidth: 4,
    trailWidth: 1,
    easing: 'easeInOut',
    duration: 8000,
    text: {
        autoStyleContainer: false
    },
    from: { color: '#3e8cbd', width: 1 },
    to: { color: '#3e8cbd', width: 4 },
    // Set default step function for all animate calls
    step: function(state, circle) {
        circle.path.setAttribute('stroke', state.color);
        circle.path.setAttribute('stroke-width', state.width);

        var value = Math.round(circle.value() * 600);
        if (value === 0) {
            circle.setText('');
        } else {
            circle.setText(value);
        }

    }
});
bar.text.style.fontFamily = '"Heavy",  sans-serif';
bar.text.style.fontSize = '80px';

bar.animate(1.0);  // Number from 0.0 to 1.0



/* PROGRES BAR CIRCLE 3 */


var bar = new ProgressBar.Circle(box3, {
    color: '#3e8cbd',
    // This has to be the same size as the maximum width to
    // prevent clipping
    strokeWidth: 4,
    trailWidth: 1,
    easing: 'easeInOut',
    duration: 7500,
    text: {
        autoStyleContainer: false
    },
    from: { color: '#3e8cbd', width: 1 },
    to: { color: '#3e8cbd', width: 4 },
    // Set default step function for all animate calls
    step: function(state, circle) {
        circle.path.setAttribute('stroke', state.color);
        circle.path.setAttribute('stroke-width', state.width);

        var value = Math.round(circle.value() * 60);
        if (value === 0) {
            circle.setText('');
        } else {
            circle.setText(value);
        }

    }
});
bar.text.style.fontFamily = '"Heavy",  sans-serif';
bar.text.style.fontSize = '80px';

bar.animate(1.0);  // Number from 0.0 to 1.0