<?php

namespace app\models\FeedbackFile;

use Yii;
use app\models\Feedback\Feedback;

/**
 * This is the model class for table "ProjectFile".
 *
 * @property integer $ID
 * @property integer $ProjectID
 * @property string $File
 *
 * @property Project $project
 */
class FeedbackFile extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'FeedbackFile';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['FeedbackID', 'File'], 'required'],
            [['FeedbackID'], 'integer'],
            [['File'], 'string', 'max' => 255],
            [['FeedbackID'], 'exist', 'skipOnError' => true, 'targetClass' => Feedback::className(), 'targetAttribute' => ['FeedbackID' => 'ID']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'ID' => Yii::t('app', 'ID'),
            'FeedbackID' => Yii::t('app', 'Feedback ID'),
            'File' => Yii::t('app', 'File'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getFeedback()
    {
        return $this->hasOne(Feedback::className(), ['ID' => 'FeedbackID']);
    }
    
      /**
     * @return \yii\db\ActiveQuery
     */
    public function getFile()
    {
        return Yii::getAlias("@web/uploads/feedback/" . $this->File);
    }
}
