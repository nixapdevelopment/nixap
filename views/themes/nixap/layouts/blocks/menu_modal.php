<?php

    use yii\helpers\Html;
    use yii\helpers\Url;
    use app\assets\NixapFrontAssets;
    use app\modules\Settings\Settings;
    use app\modules\Feedback\components\FeedbackWidget\FeedbackWidget;
    
    $bundle = NixapFrontAssets::register($this);
    
?>

    <div id="test-modal" class="mfp-hide modal-menu">
        <div class="uk-clearfix">
            <div class="left-bar uk-clearfix">
                <div class="close">
                    <a class="popup-modal-dismiss" href="#">X</a>
                </div>
                <div class="socio-bar">
                    
                    <?php if (Settings::getByName('facebookLink')) { ?>
                    <a class="facebook" target="_blank" href="<?= Settings::getByName('facebookLink') ?>">
                        <i class="uk-icon-facebook-f"></i>
                    </a>
                    <?php } ?>                    
                    <?php if (Settings::getByName('instagramLink')) { ?>
                    <a class="instagram" target="_blank" href="<?= Settings::getByName('instagramLink') ?>">
                        <i class="uk-icon-instagram"></i>
                    </a>
                    <?php } ?>
                    <?php if (Settings::getByName('ytLink')) { ?>
                    <a class="youtube" target="_blank" href="<?= Settings::getByName('ytLink') ?>">
                        <i class="uk-icon-youtube"></i>
                    </a>
                    <?php } ?>
                    
                </div>
            </div>
            <div class="uk-grid">
                <div class="uk-width-1-2 hover-bg">
                    <div class="boxes">
                        <div class="uk-grid">
                            <div class="uk-width-1-2 test1">
                                <div class="parent-box">
                                    <div class="menu-box box1">
                                        <a href="<?= Url::to(['/portfolio']) ?>">
                                            Portfoliu.
                                        </a>
                                        <a class="toggle-bottom1" href="#">
                                            <i class="uk-icon-navicon"></i>
                                        </a>
                                        <ul class="bubble-right button first-list">
                                            <li>
                                                <a href="#">
                                                    Lorem ipsum
                                                </a>
                                            </li>
                                            <li>
                                                <a href="#">
                                                    Lorem ipsum
                                                </a>
                                            </li>
                                            <li>
                                                <a href="#">
                                                    Lorem ipsum
                                                </a>
                                            </li>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                            <div class="uk-width-1-2 test2">
                                <div class="parent-box2">
                                    <div class="menu-box box2">
                                        <a class="bubble-right button" href="<?= Url::to(['/despre-noi']) ?>">
                                            Despre noi.
                                        </a>
                                        <a class="toggle-bottom2" href="#">
                                            <i class="uk-icon-navicon"></i>
                                        </a>
                                        <ul class="bubble-right button secound-list">
                                            <li>
                                                <a href="#">
                                                    Lorem ipsum
                                                </a>
                                            </li>
                                            <li>
                                                <a href="#">
                                                    Lorem ipsum
                                                </a>
                                            </li>
                                            <li>
                                                <a href="#">
                                                    Lorem ipsum
                                                </a>
                                            </li>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                            <div class="uk-width-1-2">
                                <div class="parent-box3">
                                    <div class="menu-box box3">
                                        <a class="bubble-right button" href="<?= Url::to(['/services']) ?>">
                                            Ce facem.
                                        </a>
                                        <a class="toggle-bottom3" href="#">
                                            <i class="uk-icon-navicon"></i>
                                        </a>
                                        <ul class="bubble-right button thirdth-list">
                                            <li>
                                                <a href="#">
                                                    Lorem ipsum
                                                </a>
                                            </li>
                                            <li>
                                                <a href="#">
                                                    Lorem ipsum
                                                </a>
                                            </li>
                                            <li>
                                                <a href="#">
                                                    Lorem ipsum
                                                </a>
                                            </li>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                            <div class="uk-width-1-2">
                                <div class="parent-box4">
                                    <div class="menu-box box4">
                                        <a class="bubble-right button" href="<?= Url::to(['/projects']) ?>">
                                            Proces.
                                        </a>
                                        <a class="toggle-bottom4" href="#">
                                            <i class="uk-icon-navicon"></i>
                                        </a>
                                        <ul class="bubble-right button fourth-list">
                                            <li>
                                                <a href="#">
                                                    Lorem ipsum
                                                </a>
                                            </li>
                                            <li>
                                                <a href="#">
                                                    Lorem ipsum
                                                </a>
                                            </li>
                                            <li>
                                                <a href="#">
                                                    Lorem ipsum
                                                </a>
                                            </li>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                            <div class="uk-width-1-2">
                                <div class="parent-box5">
                                    <div class="menu-box box5">
                                        <a class="bubble-right button" href="#">
                                            Produse.
                                        </a>
                                        <a class="toggle-bottom5" href="#">
                                            <i class="uk-icon-navicon"></i>
                                        </a>
                                        <ul class="bubble-right button fiveth-list">
                                            <li>
                                                <a href="#">
                                                    Lorem ipsum
                                                </a>
                                            </li>
                                            <li>
                                                <a href="#">
                                                    Lorem ipsum
                                                </a>
                                            </li>
                                            <li>
                                                <a href="#">
                                                    Lorem ipsum
                                                </a>
                                            </li>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                            <div class="uk-width-1-2">
                                <div class="parent-box6">
                                    <div class="menu-box box6">
                                        <a class="bubble-right button" href="<?= Url::to(['/blog']) ?>">
                                            Blog.
                                        </a>
                                        <a class="toggle-bottom6" href="#">
                                            <i class="uk-icon-navicon"></i>
                                        </a>
                                        <ul class="bubble-right button sixth-list">
                                            <li>
                                                <a href="#">
                                                    Lorem ipsum
                                                </a>
                                            </li>
                                            <li>
                                                <a href="#">
                                                    Lorem ipsum
                                                </a>
                                            </li>
                                            <li>
                                                <a href="#">
                                                    Lorem ipsum
                                                </a>
                                            </li>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="uk-width-1-2 uk-padding-remove hover-bg2">
                    <div class="form">
                        
                        <?= FeedbackWidget::widget() ?>
                        
                        <div class="contacts-box">
                            <div class="uk-grid">
                                <div class="uk-width-1-2">
                                    <a class="call" href="tel:+373<?= Settings::getByName('phone') ?>">
                                        +373 <span><?= Settings::getByName('phone') ?></span>
                                    </a>
                                    <a class="adress">
                                        <?= Settings::getByName('address', true) ?>
                                    </a>
                                    <a class="mail" href="mailto:<?= Settings::getByName('email') ?> ">
                                        <?= Settings::getByName('email') ?> 
                                    </a>
                                </div>
                                <div class="uk-width-1-2">
                                    <a class="all-contact" href="<?= Url::to(['/contacte']) ?>">
                                        Toate contactele
                                    </a>
                                </div>
                            </div>
                        </div>
                        <div class="copyright">
                            <div class="uk-grid">
                                <div class="uk-width-1-1 uk-text-center">
                                    <p>
                                        &copy Copyright creat de
                                        <a href="#">
                                            Nixap
                                        </a>
                                    </p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>