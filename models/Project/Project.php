<?php

namespace app\models\Project;

use Yii;
use app\models\ProjectFile\ProjectFile;

/**
 * This is the model class for table "Project".
 *
 * @property integer $ID
 * @property string $Name
 * @property string $Phone
 * @property string $Description
 * @property string $Date
 *
 * @property ProjectFile[] $projectFiles
 */
class Project extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'Project';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['Name', 'Phone', 'Description'], 'required'],
            [['Description'], 'string'],
            [['Date'], 'safe'],
            [['Name', 'Phone'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'ID' => Yii::t('app', 'ID'),
            'Name' => Yii::t('app', 'Name'),
            'Phone' => Yii::t('app', 'Phone'),
            'Description' => Yii::t('app', 'Description'),
            'Date' => Yii::t('app', 'Date'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getProjectFiles()
    {
        return $this->hasMany(ProjectFile::className(), ['ProjectID' => 'ID']);
    }
}
