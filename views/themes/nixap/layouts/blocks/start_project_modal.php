<?php

    use yii\helpers\Html;
    use yii\helpers\Url;
    use app\assets\NixapFrontAssets;
    use app\modules\Settings\Settings;
    use app\modules\Feedback\components\FeedbackWidget\FeedbackWidget;
    
    $bundle = NixapFrontAssets::register($this);
    
?>

    <div id="test-form" class="uk-modal">
        <div class="proiect-form uk-modal-dialog">
            <a class="uk-modal-close uk-close">X</a>
            <div class="form">
                
                <?= FeedbackWidget::widget() ?>
               
            </div>
        </div>
    </div>
 