<?php

use yii\bootstrap\Html;
use yii\helpers\Url;
use app\modules\Article\components\ArticleImageWidget\ArticleImageWidget;
use app\modules\Article\components\ArticleFileWidget\ArticleFileWidget;
use yii\widgets\ListView;
use yii\widgets\Pjax;
use app\models\Article\Article;
use yii\data\ActiveDataProvider;
use app\modules\Settings\Settings;
use app\modules\Newslatter\components\NewslatterWidget\NewslatterWidget;

$this->title = $article->lang->Title;

$query = Article::getDb()->cache(function ($db) {
    return Article::find()->with(['lang', 'link'])->where(['Type' => 'Blog', 'Status' => 'Active'])->orderBy('Date DESC');
}, 60);

$latestBlogs = Article::getDb()->cache(function ($db) {
    return Article::find()->with(['lang', 'link'])->where(['Type' => 'Blog', 'Status' => 'Active'])->orderBy('Date DESC')->limit(10)->all();
}, 60);

$articlesPerPage = Settings::getByName('articlesPerPage');

$dataProvider = new ActiveDataProvider([
    'query' => $query,
    'pagination' => [
        'pageSize' => $articlesPerPage
    ],
]);
    
?>

        <div class="wrapper uk-clearfix portfolio blog">
          
            <?= $this->render("../layouts/blocks/header_block") ?>
                   
            <div class="uk-clearfix our-portfolio blog">
                <div class="uk-container-center uk-container">
                    <div class="uk-grid">
                        <div class="uk-width-medium-3-10">
                            <div class="uk-width-1-1">
                                <div class="left-sidebar">
                                    <div class="title">
                                        <h4>
                                            Blog.
                                        </h4>
                                    </div>
                                    <div class="blog-dropdown">
                                        <a href="#">
                                            <i class="uk-icon-navicon"></i>
                                        </a>
                                    </div>
                                     <?php if (count($latestBlogs) > 0) { ?>
                                    <ul>
                                        <?php foreach ($latestBlogs as $blog) { ?>
                                            <li>
                                                <a href="<?= $blog->seoLink ?>">
                                                    <?= $blog->lang->Title ?>
                                                </a>
                                            </li>                                           
                                        <?php } ?>
                                    </ul>
                                    <?php } ?>
                                    
                                    
                                </div>
                                
                                
                                
                                <div class="left-sidebar sidebar2 mt20">
                                    <div class="title">
                                        <h4>
                                            <?= Yii::t("app", "Aboneazăt-te la Newslatter.") ?>
                                        </h4>
                                    </div>
                                    
                                    <?= NewslatterWidget::widget() ?>
                                    
                                </div>
                            </div>
                        </div>
                        <div class="uk-width-medium-7-10">
                            <div class="uk-grid">
                                
                                <?php Pjax::begin(['options' => ['tag'=>false]]); ?>
                                    <?= ListView::widget([
                                        'dataProvider' => $dataProvider,
                                        'itemView' => function ($model, $key, $index, $widget){
                                        if ($index == 1 || $index == 5)
                                            {
                                                return $this->render('blog_list_item_large', [
                                                    'model' => $model
                                                ]);
                                            }
                                            return $this->render('blog_list_item', [
                                                'model' => $model
                                            ]);
                                        },
                                        'layout' => '{items}',
                                        'options' => ['tag'=>false],
                                        'itemOptions' => ['tag'=>false],

                                    ]); ?>
                                <?php Pjax::end(); ?> 
                                
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <?= $this->render("../layouts/blocks/footer_block") ?>

        </div>
