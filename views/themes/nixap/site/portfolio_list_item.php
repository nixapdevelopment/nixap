<div class="uk-width-medium-3-10" data-pjax="0">
    <div class="middle-description">
        <div class="img">
            <a href="<?= $model->seoLink ?>">
                <?= yii\helpers\Html::img($model->MainThumbUrl) ?>
            </a>
        </div>
        <div class="description">
            <h4>
                <a href="<?= $model->seoLink ?>">
                    <?= $model->lang->Title ?>
                </a>
            </h4>
            <p>
                <?= $model->getShortText(300) ?>
            </p>
        </div>
    </div>
</div>
