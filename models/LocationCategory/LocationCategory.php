<?php

namespace app\models\LocationCategory;

use Yii;
use app\models\Category\Category;

/**
 * This is the model class for table "LocationCategory".
 *
 * @property integer $ID
 * @property integer $LocationID
 * @property integer $CategoryID
 */
class LocationCategory extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'LocationCategory';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['LocationID', 'CategoryID'], 'required'],
            [['LocationID', 'CategoryID'], 'integer'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'ID' => 'ID',
            'LocationID' => 'Location ID',
            'CategoryID' => 'Category ID',
        ];
    }
    
    public function getCategory()
    {
        return $this->hasOne(Category::className(), ['ID' => 'CategoryID'])->with('currentLang');
    }
    
}
