<div class="uk-width-1-3"  data-pjax="0">
    <div class="testimonial-box">
        <div class="box uk-clearfix">
            <h5>
                Nume Prenume
            </h5>
            <p>
                Funcție
            </p>
            <div class="nav-button">
                <a href="<?= $model->seoLink ?>">
                    <img src="images/pdf-file-format-symbol.png" alt="">
                </a>
                <a href="https://vimeo.com/45830194" class="popup-video">
                    <img src="images/youtube-(1).png" alt="">
                </a>
            </div>
        </div>
        <div class="box-description">
            <h5>
                <?= $model->lang->Title ?>
            </h5>
            <p>
                <?= $model->getShortText(300) ?>
            </p>
        </div>
    </div>
</div>

