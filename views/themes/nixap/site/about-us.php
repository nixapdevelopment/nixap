<?php

use yii\bootstrap\Html;
use yii\helpers\Url;
use app\modules\Article\components\ArticleImageWidget\ArticleImageWidget;
use app\modules\Article\components\ArticleFileWidget\ArticleFileWidget;
use app\models\SliderItem\SliderItem;
use yii\caching\TagDependency;
use app\assets\NixapFrontAssets;

$bundle = NixapFrontAssets::register($this);

$this->title = $article->lang->Title;

$slides = SliderItem::getDb()->cache(function ($db) {
    return SliderItem::find()->with('lang')->where(['SliderID' => 2])->all();
}, 30, new TagDependency(['tags' => 'slider']));
    
?>

<div class="wrapper uk-clearfix portfolio despre-noi">
    
    <?= $this->render("../layouts/blocks/header_block") ?>

        <div class="uk-clearfix about-they">
            <div class="uk-container-center uk-container">
                <div class="uk-grid">
                    <div class="uk-width-medium-3-10">
                        <div class="left-sidebar">
                            <div class="title">
                                <h4>
                                    Despre noi.
                                </h4>
                            </div>
                            <ul>
                                <li>
                                    <a href="<?= Url::to(['/testimoniale']) ?>">
                                        <?= Yii::t("app", "Testimoniale") ?>
                                    </a>
                                </li>
                                <li>
                                    <a href="<?= Url::to(['/echipa']) ?>">
                                        <?= Yii::t("app", "Echipa") ?>
                                    </a>
                                </li>
                            </ul>
                        </div>
                    </div>
                    <div class="uk-width-medium-7-10">
                        <div class="description-box">
                            <div class="title-section">
                                <h3>
                                    Lorem ipsum dolor sit amet,
                                    consectetuer adipiscing elit.
                                </h3>
                            </div>
                            <div class="paragraph">
                                <p>
                                    Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Donec odio. Quisque
                                    volutpat mattis eros. Nullam malesuada erat ut turpis. Suspendisse urna nibh,
                                    viverra non, semper suscipit, posuere a, pede.
                                </p>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="uk-grid">
                    <div class="uk-width-medium-7-10 resize-slider">
                        
                        <?php if (count($slides) > 0) { ?>
                        <div class="our-slider2">
                            <div class="owl-carousel slider2 owl-theme">
                            <?php foreach ($slides as $slide) { ?>
                                <div class="item">
                                    <?= Html::img($slide->imageUrl) ?>
                                </div>
                            <?php } ?>
                            </div>
                        </div>
                        <?php } ?>
                                                    
                    </div>
                    <div class="uk-width-medium-3-10 resize-box">
                        <div class="description-slide">
                            <div class="slide-box-description">
                                <h4>
                                    Cras ornare tristique elit.
                                </h4>
                                <p>
                                    Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Donec odio.
                                    Quisque volutpat mattis eros. Nullam malesuada erat ut turpis. Suspendisse urna
                                    nibh, viverra non, semper suscipit, posuere a, pede.
                                </p>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="uk-grid">
                    <div class="uk-width-1-1">
                        <div class="description-box">
                            <div class="title">
                                <h3>
                                    Lorem ipsum dolor sit amet, consectetuer
                                    adipiscing elit.
                                </h3>
                            </div>
                            <div class="description">
                                <p>
                                    Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Donec odio.
                                    Quisque volutpat mattis eros. Nullam malesuada erat ut turpis. Suspendisse
                                    urna nibh, viverra non, semper suscipit, posuere a, pede. Lorem ipsum dolor sit
                                    amet, consectetuer adipiscing elit. Donec odio. Quisque volutpat mattis eros.
                                    Nullam malesuada erat ut turpis. Suspendisse urna nibh, viverra non, semper
                                    suscipit, posuere a, pede. Lorem ipsum dolor sit amet, consectetuer adipiscing
                                    elit. Donec odio. Quisque volutpat mattis eros. Nullam malesuada erat ut turpis.
                                    Suspendisse urna nibh, viverra non, semper suscipit, posuere a, pede.
                                </p>
                                <p>
                                    Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Donec odio.
                                    Quisque volutpat mattis eros. Nullam malesuada erat ut turpis. Suspendisse
                                    urna nibh, viverra non, semper suscipit, posuere a, pede.
                                </p>
                            </div>
                        </div>
                        <div class="call-them uk-text-center mt80">
                            <a class="btn" href="<?= Url::to(['/contacte']) ?>">
                                CONTACTEAZĂ-NE.
                            </a>
                        </div>
                    </div>
                </div>
                <div class="uk-grid mt80">
                    <div class="uk-width-1-2">
                        <div class="post-box">
                            <div class="img">
                                <a href="#">
                                    <?= Html::img($bundle->baseUrl . '/images/Layer-1-1.jpg') ?>
                                </a>
                            </div>
                            <div class="description">
                                <h4>
                                    <a href="#">
                                        Sed adipiscing ornare risus.
                                    </a>
                                </h4>
                                <p>
                                    Praesent dapibus, neque id cursus faucibus, tortor neque
                                    egestas augue, eu vulputate magna eros eu erat. Aliquam erat volutpat.
                                    Nam dui mi, tincidunt quis, accumsan porttitor, facilisis luctus, metus.
                                </p>
                            </div>
                        </div>
                    </div>
                    <div class="uk-width-1-2">
                        <div class="uk-grid">
                            <div class="uk-width-1-2">
                                <div class="box">
                                    <div class="img">
                                        <a href="#">
                                            <?= Html::img($bundle->baseUrl . '/images/Layer-2.jpg') ?>
                                        </a>
                                    </div>
                                    <div class="description">
                                        <h4>
                                            <a href="#">
                                                Pellentesque fermentum
                                                dolor.
                                            </a>
                                        </h4>
                                        <p>
                                            Sed adipiscing ornare risus. Morbi est est, blandit sit amet,
                                            sagittis vel, euismod vel, velit. Pellentesque egestas sem.
                                            Suspendisse commodo ullamcorper magna.
                                        </p>
                                    </div>
                                </div>
                            </div>
                            <div class="uk-width-1-2">
                                <div class="box">
                                    <div class="img">
                                        <a href="#">
                                            <?= Html::img($bundle->baseUrl . '/images/Layer-2.jpg') ?>
                                        </a>
                                    </div>
                                    <div class="description">
                                        <h4>
                                            <a href="#">
                                                Pellentesque fermentum
                                                dolor.
                                            </a>
                                        </h4>
                                        <p>
                                            Sed adipiscing ornare risus. Morbi est est, blandit sit amet,
                                            sagittis vel, euismod vel, velit. Pellentesque egestas sem.
                                            Suspendisse commodo ullamcorper magna.
                                        </p>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <?= $this->render("../layouts/blocks/footer_block") ?>

</div>