<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\Pjax;
/* @var $this yii\web\View */
/* @var $searchModel app\models\Gallery\GallerySearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('app', 'Galleries');
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="gallery-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a(Yii::t('app', 'Create Gallery'), ['create'], ['class' => 'btn btn-success']) ?>
    </p>
<?php Pjax::begin(); ?>    
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        //'filterModel' => $searchModel,
        'columns' => [
            [
                'class' => 'yii\grid\SerialColumn',
                'options' => [
                    'width' => '50',
                ],
            ],

            [
                'attribute' => 'lang.Title',
            ],
            [
                'label' => Html::tag('div', Yii::t('app', 'Gallery items'), [
                    'class' => 'text-center'
                ]),
                'value' => function($model)
                {
                    return Html::tag('div', $model->countItems, [
                        'class' => 'text-center'
                    ]);
                },
                'options' => [
                    'width' => '100'
                ],
                'encodeLabel' => false,
                'format' => 'raw',
            ],

            [
                'class' => 'yii\grid\ActionColumn',
                'template' => '<div class="text-center">{update}&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;{delete}</div>',
                'options' => [
                    'width' => '100'
                ]
            ],
        ],
    ]); ?>
<?php Pjax::end(); ?></div>
