<div class="uk-width-medium-3-10" data-pjax="0">
    <div class="middle-description">
        <div class="img">
            <a href="<?= $model->seoLink ?>">
                <?= yii\helpers\Html::img($model->MainThumbUrl) ?>
            </a>
            <div class="date-box">
                <span class="date">
                    <?= date("d", strtotime($model->Date)) ?>
                </span>
                <span class="moth">
                    <?= date("F", strtotime($model->Date)) ?>
                </span>
                <span class="year">
                    <?= date("Y", strtotime($model->Date)) ?>
                </span>
            </div>
        </div>
        <div class="description">
            <h4>
                <a href="<?= $model->seoLink ?>">
                    <?= $model->lang->Title ?>
                </a>
            </h4>
            <p>
                <?= $model->getShortText(300) ?>
            </p>
        </div>
    </div>
</div>
