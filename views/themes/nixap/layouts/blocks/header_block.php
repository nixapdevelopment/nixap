<?php

    use yii\helpers\Html;
    use yii\helpers\Url;
    use app\assets\NixapFrontAssets;
    use app\modules\Settings\Settings;
    
    $bundle = NixapFrontAssets::register($this);
    

?>

<div class="page">
    <div class="menu">
        <a href="#test-modal" class="popup-modal">
                <span>

                </span>
                <span>

                </span>
                <span>

                </span>
        </a>
    </div>
    <div class="uk-clearfix header">
        <div class="uk-container-center uk-container">
            <div class="uk-grid">
                <div class="uk-width-1-2">
                    <div class="table">
                        <div class="middle">
                            <h1>
                                <a href="<?= Url::to(['/']) ?>">
                                    NIXAP
                                </a>
                            </h1>
                        </div>
                        <div class="middle">
                            <?= Settings::getByName('topBarText', true) ?> 
                        </div>
                    </div>
                </div>
                <div class="uk-width-1-2 uk-clearfix">
                    <div class="contact">
                        <div class="table">
                            <div class="middle left">
                                <p>
                                    <a href="tel:+373<?= Settings::getByName('phone') ?>">
                                        +373
                                        <span>
                                            <?= Settings::getByName('phone') ?>
                                        </span>
                                    </a>
                                </p>
                            </div>
                            <div class="middle">
                                <a class="btn start-proiect" href="#test-form" data-uk-modal>
                                    <?= Yii::t("app", "START PROIECT") ?>
                                </a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
