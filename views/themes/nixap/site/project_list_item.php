<div class="uk-width-medium-1-4" data-pjax="0">
    <div class="box">
        <div class="img">
            <a href="<?= $model->seoLink ?>">
                <?= yii\helpers\Html::img($model->MainThumbUrl) ?>
            </a>
        </div>
        <div class="description">
            <h5>
                <a href="<?= $model->seoLink ?>">
                    <?= $model->lang->Title ?>
                </a>
            </h5>
            <p>
                <?= $model->getShortText(300) ?>
            </p>
        </div>
    </div>
</div>
