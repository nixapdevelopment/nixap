<?php

namespace app\assets;

use yii\web\AssetBundle;


class NixapFrontAssets extends AssetBundle
{
    
    public $sourcePath = '@app/views/themes/nixap/assets';
    
    public $css = [
        'css/font-awesome.min.css',
        'css/uikit.min.css',
        'css/uikit.almost-flat.min.css',
        'css/animate.css',
        'js/owlcarousel/assets/owl.carousel.min.css',
        'js/owlcarousel/assets/owl.theme.default.min.css',
        'css/magnific-popup.css',  
        'css/twentytwenty.css',
        'css/main.css',
    ];
    
    public $js = [
        //'js/jquery.js',
        'js/uikit.min.js',
        'js/jquery.magnific-popup.min.js',
        'js/wow.min.js',
        'js/owlcarousel/owl.carousel.min.js',
        'js/sticky.min.js',        
        'js/progressbar.min.js',
        'js/progressbar.js',
        'js/jquery.twentytwenty.js',
        'js/jquery.event.move.js',
        'js/my-slider.js',
        'js/scripts.js',
    ];
    
    public $depends = [
        'yii\web\YiiAsset',
        'yii\web\JqueryAsset',
    ];
    
}