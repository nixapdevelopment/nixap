<?php

use yii\bootstrap\Html;
use app\assets\NixapFrontAssets;
use yii\helpers\Url;
use app\modules\Article\components\ArticleImageWidget\ArticleImageWidget;
use app\modules\Article\components\ArticleFileWidget\ArticleFileWidget;
use yii\widgets\ListView;
use yii\widgets\Pjax;
use app\models\Article\Article;
use yii\data\ActiveDataProvider;
use app\modules\Settings\Settings;

$bundle = NixapFrontAssets::register($this);

$this->title = $article->lang->Title;

$query = Article::getDb()->cache(function ($db) {
    return Article::find()->with(['lang', 'link'])->where(['Type' => 'Testimonial', 'Status' => 'Active'])->orderBy('Date DESC');
}, 60);

$latestArticles = Article::getDb()->cache(function ($db) {
    return Article::find()->with(['lang', 'link'])->where(['Type' => 'Testimonial', 'Status' => 'Active'])->orderBy('Date DESC')->limit(10)->all();
}, 60);

$articlesPerPage = Settings::getByName('articlesPerPage');

$dataProvider = new ActiveDataProvider([
    'query' => $query,
    'pagination' => [
        'pageSize' => $articlesPerPage
    ],
]);
    
?>

<div class="wrapper uk-clearfix portfolio">
    
    <?= $this->render("../layouts/blocks/header_block") ?>
    
        <div class="uk-clearfix our-portfolio blog testimonial">
            <div class="uk-container-center uk-container">
                <div class="uk-grid">
                    <div class="uk-width-medium-3-10">
                        <div class="uk-width-1-1">
                            <div class="left-sidebar">
                                <div class="title">
                                    <h4>
                                        <?= $article->lang->Title ?>
                                    </h4>
                                </div>
                                <ul>
                                    <li class="active">
                                        <a href="#">
                                            Despre noi
                                        </a>
                                    </li>
                                </ul>
                            </div>
                            <div class="left-sidebar mt20 sidebar2 sidebar4 uk-padding-remove">
                                <div class="video">
                                    <?= Html::img($bundle->baseUrl . "/images/Layer-1-2.jpg") ?>
                                    <a href="https://vimeo.com/45830194" class="popup-video">
                                        <i class="uk-icon-play-circle-o"></i>
                                    </a>
                                </div>
                                <div class="title-video">
                                    <p>
                                        Integer vitae libero ac risus egestas placerat.
                                    </p>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="uk-width-medium-7-10">
                        <div class="uk-grid">
                            
                            <?php Pjax::begin(['options' => ['tag'=>false]]); ?>
                                <?= ListView::widget([
                                    'dataProvider' => $dataProvider,
                                    'itemView' => 'testimonial_list_item',
                                    'layout' => '{items}',
                                    'options' => ['tag'=>false],
                                    'itemOptions' => ['tag'=>false],

                                ]); ?>
                            <?php Pjax::end(); ?> 
                            
                            
                        </div>
                    </div>
                </div>
            </div>
        </div>
        
    <?= $this->render("../layouts/blocks/footer_block") ?>

</div>