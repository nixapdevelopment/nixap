<?php

namespace app\modules\Article\controllers;

use Yii;
use app\controllers\BackendController;
use yii\base\Model;
use app\models\Article\Article;
use app\models\ArticleLang\ArticleLang;
use app\models\Article\ArticleSearch;
use app\models\ArticleImage\ArticleImage;
use app\models\ArticleImageLang\ArticleImageLang;
use yii\web\UploadedFile;
use app\models\ArticleFile\ArticleFile;
use dosamigos\transliterator\TransliteratorHelper;
use app\models\ArticleRelation\ArticleRelation;

/**
 * Default controller for the `Article` module
 */
class ArticleController extends BackendController
{
    
    public function actionIndex($type = 'Page')
    {
        $articleConfig = $this->module->getArticleConfig($type);
        
        $searchModel = new ArticleSearch(['Type' => $type]);
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams, $articleConfig);

        return $this->render('index', [
            'articleConfig' => $articleConfig,
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }
    
    public function actionCreate($type = 'Page')
    {
        $articleConfig = $this->module->getArticleConfig($type);
        
        $model = new Article();
        $model->Type = $type;
        
        $langModels = [];
        foreach (Yii::$app->params['siteLanguages'] as $i => $lang)
        {
            $langModels[$i] = new ArticleLang([
                'LangID' => $lang
            ]);
        }
        
        if (Yii::$app->request->isPost)
        {
            if ($model->load(Yii::$app->request->post()) && $model->validate() && Model::loadMultiple($langModels, Yii::$app->request->post()) && Model::validateMultiple($langModels))
            {
                $model->save();
                
                foreach ($langModels as $langModel)
                {
                    $langModel->ArticleID = $model->ID;
                    $langModel->save();
                }
                
                $this->createSlug($model);
                
                // save relations
                $relatedIDs = (array)Yii::$app->request->post('parents');
                ArticleRelation::deleteAll(['ChildArticleID' => $model->ID]);
                foreach ($relatedIDs as $rID)
                {
                    $arModel = new ArticleRelation([
                        'ParentArticleID' => $rID,
                        'ChildArticleID' => $model->ID,
                    ]);
                    $arModel->save();
                }
                
                
                Yii::$app->session->setFlash('success', $articleConfig['labels']['wasSaved']);
                
                if (Yii::$app->request->post('Redirect') == 'list')
                {
                    return $this->redirect($this->url(['index'], $model->Type));
                }
                return $this->redirect($this->url(['update', 'id' => $model->ID], $model->Type));
            }
        }
        
        $all_articles = Article::find()->where(['Type' => $type])->all();
        $related_articles = [];
        
        return $this->render('create', [
            'model' => $model,
            'langModels' => $langModels,
            'all_articles' => $all_articles,
            'related_articles' => $related_articles,
            'articleConfig' => $articleConfig,
        ]);
    }
    
    public function actionUpdate($id, $type = 'Page')
    {
        $articleConfig = $this->module->getArticleConfig($type);
        
        $model = $this->findModel($id);
        
        $langModels = [];
        foreach (Yii::$app->params['siteLanguages'] as $i => $lang)
        {
            $langModels[$i] = isset($model->langs[$lang]) ? $model->langs[$lang] : new ArticleLang([
                'LangID' => $lang,
                'ArticleID' => $model->ID,
            ]);
        }
        
        if (Yii::$app->request->isPost)
        {
            if ($model->load(Yii::$app->request->post()) && $model->validate() && Model::loadMultiple($langModels, Yii::$app->request->post()) && Model::validateMultiple($langModels))
            {
                $model->save();
                
                foreach ($langModels as $langModel)
                {
                    $langModel->save();
                }
                
                $this->createSlug($model);
                
                // save relations
                $relatedIDs = (array)Yii::$app->request->post('parents');
                ArticleRelation::deleteAll(['ChildArticleID' => $model->ID]);
                foreach ($relatedIDs as $rID)
                {
                    $arModel = new ArticleRelation([
                        'ParentArticleID' => $rID,
                        'ChildArticleID' => $model->ID,
                    ]);
                    $arModel->save();
                }
                
                Yii::$app->session->setFlash('success', $articleConfig['labels']['wasSaved']);
                
                if (Yii::$app->request->post('Redirect') == 'list')
                {
                    return $this->redirect($this->url(['index'], $model->Type));
                }
                return $this->redirect($this->url(['update', 'id' => $model->ID], $model->Type));
            }
        }
        
        $all_articles = Article::find()->where(['Type' => $type])->andWhere(['<>', 'ID', $model->ID])->all();
        $related_articles = ArticleRelation::find()->where(['ChildArticleID' => $model->ID])->all();
        
        return $this->render('update', [
            'model' => $model,
            'langModels' => $langModels,
            'all_articles' => $all_articles,
            'related_articles' => $related_articles,
            'articleConfig' => $articleConfig,
        ]);
    }
    
    public function actionDelete($id)
    {
        $model = Article::findOne($id);
        $model->delete();
        
        return $this->redirect($this->url(['index'], $model->Type));
    }

    /**
     * @return app\models\Article\Article
     */
    public function findModel($id)
    {
        if (($model = Article::find()->with(['langs'])->where(['ID' => $id])->one()) !== null)
        {
            return $model;
        } 
        else
        {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
    
    public function createSlug($model)
    {
        $slug = TransliteratorHelper::process(reset($model->langs)->Title, '');
        $slug = str_replace(' ', '-', trim(strtolower($slug)));
        
        $slugModel = $model->link;
        
        if ($slugModel != NULL)
        {
            $slugModel->Link = $slug;
        }
        else
        {
            $slugModel = new \app\models\Link\Link([
                'ArticleID' => $model->ID,
                'Link' => $slug
            ]);
        }
        
        $slugModel->save();
    }

    public function url($url, $type)
    {
        $url = (array)$url;
        $url['type'] = $type;
        return \yii\helpers\Url::to($url);
    }
    
    public function actionUploadImage()
    {
        $articleID = (int)Yii::$app->request->post('ArticleID');
        $images = \yii\web\UploadedFile::getInstancesByName('Images');
        
        $maxPosition = ArticleImage::find()->where(['ArticleID' => $articleID])->max('Position');
        $newPosition = $maxPosition == NULL ? 0 : $maxPosition ++;

        $uploadedIDs = [];
        foreach ($images as $imageIns)
        {
            $image = md5(microtime(true)) . '.' . $imageIns->extension;
            $imagePath = Yii::getAlias('@webroot/uploads/article/' . $image);
            
            if ($imageIns->saveAs($imagePath))
            {
                \yii\imagine\Image::thumbnail($imagePath, 1200, 900)->save($imagePath);
                
                $thumb = 'thumb_' . $image;
                $thumbPath = Yii::getAlias('@webroot/uploads/article/' . $thumb);
                \yii\imagine\Image::thumbnail($imagePath, 400, 300)->save($thumbPath);
                
                $articleImage = new ArticleImage();
                $articleImage->ArticleID = $articleID;
                $articleImage->Thumb = $thumb;
                $articleImage->Image = $image;
                $articleImage->Position = $newPosition;
                $articleImage->save();
                
                $uploadedIDs[] = $articleImage->ID;
                $newPosition++;
            }
        }
        
        exit(json_encode($uploadedIDs));
    }
    
    public function actionDeleteImage()
    {
        $articleImageID = Yii::$app->request->post('key');
        
        $image = ArticleImage::findOne($articleImageID);
        $image->delete();
        
        $imagesForReorder = ArticleImage::find()->where(['ArticleID' => $image->ID])->andWhere(['>', 'Position', $image->Position])->all();
        foreach ($imagesForReorder as $img)
        {
            if ($img->Position > 0)
            {
                $img->Position = $img->Position - 1;
                $img->save();
            }
        }
        
        unlink(Yii::getAlias('@webroot/uploads/article/' . $image->Image));
        unlink(Yii::getAlias('@webroot/uploads/article/' . $image->Thumb));
        
        exit("{}");
    }
    
    public function actionImageSort()
    {
        $order = Yii::$app->request->post('order');
        
        $position = 0;
        foreach ($order as $imageID)
        {
            Yii::$app->db->createCommand("UPDATE `ArticleImage` SET `Position` = '$position' WHERE `ID` = $imageID")->query();
            $position++;
        }
    }
    
    public function actionImageDescription()
    {
        if (!Yii::$app->request->isAjax) exit;
        
        $imageID = (int)Yii::$app->request->post('imageID', 0);
        
        $image = ArticleImage::find()->with('langs')->where(['ID' => $imageID])->limit(1)->one();
        
        $langModels = [];
        foreach (Yii::$app->params['siteLanguages'] as $i => $lang)
        {
            $langModels[$i] = isset($image->langs[$lang]) ? $image->langs[$lang] : new ArticleImageLang([
                'ArticleImageID' => $image->ID,
                'LangID' => $lang,
            ]);
        }
        
        return $this->renderPartial('_image_description', [
            'image' => $image,
            'langModels' => $langModels,
        ]);
    }
    
    public function actionSaveImageDescription()
    {
        if (!Yii::$app->request->isAjax) exit;
        
        $imageID = (int)Yii::$app->request->post('imageID', 0);
        
        $image = ArticleImage::find()->with('langs')->where(['ID' => $imageID])->limit(1)->one();

        if ($image != null)
        {
            $langModels = [];
            foreach (Yii::$app->params['siteLanguages'] as $i => $lang)
            {
                $langModels[$i] = isset($image->langs[$lang]) ? $image->langs[$lang] : new ArticleImageLang([
                    'ArticleImageID' => $image->ID,
                    'LangID' => $lang,
                ]);
            }

            if (Model::loadMultiple($langModels, Yii::$app->request->post()) && Model::validateMultiple($langModels))
            {
                foreach ($langModels as $lModel)
                {
                    $lModel->save();
                }
            }
        }
    }

    public function actionUploadFiles($ArticleID)
    {
        if (Yii::$app->request->isPost)
        {
            $article = Article::findOne($ArticleID);
            
            $uploadedFiles = UploadedFile::getInstancesByName('files');
            foreach ($uploadedFiles as $uploadedFile)
            {
                $fileName = TransliteratorHelper::process($uploadedFile->baseName);
                $fileName = str_replace(' ', '_', $fileName);
                $fileName = preg_replace('/[^A-Za-z0-9\-_]/', '', $fileName);
                $file = $fileName . '_' . Yii::$app->getSecurity()->generateRandomString(4) . "." . $uploadedFile->extension;
                $filePath = Yii::getAlias('@webroot/uploads/article/' . $file);
                
                if ($uploadedFile->saveAs($filePath))
                {
                    $fileModel = new ArticleFile([
                        'ArticleID' => $ArticleID
                    ]);
                    $fileModel->File = $file;
                    $fileModel->Type = \yii\helpers\FileHelper::getMimeType($filePath);
                    $fileModel->Position = 0;
                    $fileModel->save();
                }
            }
        }
        
        return $this->renderPartial('_files_form', [
            'model' => $article
        ]);
    }
    
    public function actionDeleteFile($id)
    {
        $file = ArticleFile::findOne($id);
        $file->delete();
        
        @unlink(Yii::getAlias('@webroot/uploads/article/' . $file->File));
    }
    
}