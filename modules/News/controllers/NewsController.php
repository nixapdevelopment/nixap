<?php

namespace app\modules\News\controllers;

use Yii;
use app\controllers\BackendController;
use app\models\News\News;
use app\models\News\NewsSearch;
use app\models\NewsLang\NewsLang;
use yii\base\Model;

/**
 * Default controller for the `News` module
 */
class NewsController extends BackendController
{
    
    public function actionIndex()
    {
        $searchModel = new NewsSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }
    
    public function actionCreate()
    {
        $model = new News();
        
        $pageLangModels = [];
        foreach (Yii::$app->params['siteLanguages'] as $i => $lang)
        {
            $pageLangModels[$i] = new NewsLang([
                'LangID' => $lang
            ]);
        }
        
        if (Yii::$app->request->isPost)
        {
            if ($model->load(Yii::$app->request->post()) && $model->validate() && Model::loadMultiple($pageLangModels, Yii::$app->request->post()) && Model::validateMultiple($pageLangModels))
            {
                $model->save();
                
                foreach ($pageLangModels as $pageLangModel)
                {
                    $pageLangModel->NewsID = $model->ID;
                    $pageLangModel->save();
                }
                
                Yii::$app->session->setFlash('success', 'Article was saved');
                
                return $this->redirect(['update', 'id' => $model->ID]);
            }
        }
        
        return $this->render('create', [
            'model' => $model,
            'pageLangModels' => $pageLangModels,
        ]);
    }
    
    
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);
        $pageLangModels = [];
        foreach (Yii::$app->params['siteLanguages'] as $i => $lang)
        {
            $pageLangModels[$i] = isset($model->newsLangs[$lang]) ? $model->newsLangs[$lang] : new NewsLang([
                'LangID' => $lang,
                'NewsID' => $model->ID,
            ]);
        }
        
        if (Yii::$app->request->isPost)
        {
            if ($model->load(Yii::$app->request->post()) && $model->validate() && Model::loadMultiple($pageLangModels, Yii::$app->request->post()) && Model::validateMultiple($pageLangModels))
            {
                $model->save();
                
                foreach ($pageLangModels as $pageLangModel)
                {
                    $pageLangModel->save();
                }
                
                Yii::$app->session->setFlash('success', 'Article was saved');
                
                return $this->redirect(['update', 'id' => $model->ID]);
            }
        }
        
        return $this->render('update', [
            'model' => $model,
            'pageLangModels' => $pageLangModels,
        ]);
    }
    
    public function actionDelete($id)
    {
        $model = $this->findModel($id);
        $model->delete();
        
        Yii::$app->session->setFlash('success', 'Article has been deleted');
        
        return $this->redirect(['index']);
    }

    public function findModel($id)
    {
        if (($model = News::find()->with('newsLangs')->where(['ID' => $id])->one()) !== null)
        {
            return $model;
        } 
        else
        {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
    
}