<?php

namespace app\modules\Newslatter\controllers;

use Yii;
use yii\filters\VerbFilter;
use app\controllers\BackendController;
use app\models\Newslatter\Newslatter;
use app\models\Newslatter\SearchNewslatter;

/**
 * Default controller for the `Newslatter` module
 */
class NewslatterController extends BackendController
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }
    
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
            'captcha' => [
                'class' => 'yii\captcha\CaptchaAction',
                'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null,
            ],
        ];
    }

    /**
     * Lists all Newslatter models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new SearchNewslatter();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Newslatter model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Deletes an existing Newslatter model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }
    
    public function findModel($id)
    {
        $model = Newslatter::findOne($id);
        
        if ($model == NULL)
        {
            throw new \yii\web\NotFoundHttpException("Newslatter not fount");
        }
        
        return $model;
    }
   
}
