<?php

namespace app\models\Newslatter;

use Yii;

/**
 * This is the model class for table "Newslatter".
 *
 * @property integer $ID
 * @property string $Email
 * @property string $Date
 */
class Newslatter extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'Newslatter';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['Email'], 'required'],
            [['Date'], 'safe'],
            [['Email'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'ID' => Yii::t('app', 'ID'),
            'Email' => Yii::t('app', 'Email'),
            'Date' => Yii::t('app', 'Date'),
        ];
    }
}
