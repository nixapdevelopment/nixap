<?php

/* @var $this \yii\web\View */
/* @var $content string */

use yii\bootstrap\Html;
use app\assets\NixapFrontAssets;
use app\modules\Menu\components\MenuWidget\MenuWidget;
use yii\helpers\Url;
use app\modules\Settings\Settings;
use app\models\Article\Article;
use app\models\SliderItem\SliderItem;
use yii\caching\TagDependency;

$bundle = NixapFrontAssets::register($this);

$slides = SliderItem::getDb()->cache(function ($db) {
        return SliderItem::find()->with('lang')->where(['SliderID' => 1])->all();
    }, 30, new TagDependency(['tags' => 'slider']));

?>

<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">
<head>
    <meta charset="<?= Yii::$app->charset ?>">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <?= Html::csrfMetaTags() ?>
    <link rel="shortcut icon" href="<?= Url::to('@web/uploads/settings/' . Settings::getByName('favicon')) ?>" type="image/ico">
    <title><?= Html::encode($this->title) ?></title>
    <script>var SITE_URL = '<?= Url::home(true) ?>';</script>

    <!-- Redirect to http://browsehappy.com/ on IE 8-  -->
    <!--[if lte IE 8]>
    <style type="text/css">
        body{display:none!important;}
    </style>
    <meta http-equiv="refresh" content="0; url=http://browsehappy.com/">
    <![endif]-->
    
    <?php $this->head() ?>
</head>
<body> 
<?php $this->beginBody() ?>
    
    <?= $content ?>
            
    <?= $this->render("blocks/start_project_modal") ?> 
    <?= $this->render("blocks/menu_modal") ?>
    
    
<?php $this->endBody() ?>
</body>
</html>
<?php $this->endPage() ?>
