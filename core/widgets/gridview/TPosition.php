<?php
namespace template\widgets\gridview;

use yii\bootstrap\Html;
use template\helpers\Glyphicon;

class TPosition
{
	public static function run($data)
	{
		$items = [];
		foreach($data as $item)
		{
			$items[] = static::prepareItem($item);
		}
		
		return Html::tag("ul", join('', $items), ['class' => 'position-wrap']);
	}
	
	private static function prepareItem($item)
	{
		$itemContent = static::prepareName($item);

		if($item->hasProperty('childs'))
		{
			$childs = [];
			$level = $item->level + 1;
			if(count($item->childs) > 0)
			{
				foreach($item->childs as $child)
				{
					$child->level = $level;
					$childs[] = static::prepareItem($child);
				}
			}
			
			if(count($childs) > 0)
			{
				$itemContent .= Html::tag("ul", join('', $childs));
			}
		}
		
		return Html::tag('li', $itemContent, [
			'class' => 'sortItem level-' . $item->level,
			'id' => 'item-' . $item->ID,
			'url' => $item->btns['position']['url']
		]);
	}
	
	private static function prepareName($item)
	{
		$name = Html::tag("div", $item->Name, ['class' => 'name']) . Glyphicon::Move . '<br style="clear:both"/>';
		
		return Html::tag("div", $name, ['class' => 'btn btn-default name-wrap']);
	}
}
?>