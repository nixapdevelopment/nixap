<?php

    use app\modules\Article\components\ArticleImageWidget\ArticleImageWidget;
    use app\modules\Article\components\ArticleFileWidget\ArticleFileWidget;
    use yii\helpers\Url;
    
    $this->title = $article->lang->Title;
    
?>


<div class="wrapper portfolio blog">
    
        <?= $this->render("../layouts/blocks/header_block") ?>

        <div class="uk-container-center uk-container">
            <div class="uk-grid mt30 mb30">
                <div class="uk-width-medium-3-10">
                    <div class="uk-width-1-1">
                        <div class="left-sidebar sidebar1">
                                <div class="title">
                                    <h4>
                                        Pagini.
                                    </h4>
                                </div>
                                <div class="blog-dropdown">
                                    <a href="#">
                                        <i class="uk-icon-navicon"></i>
                                    </a>
                                </div>
                                <ul>                                
                                    <li>
                                        <a href="<?= Url::to(['/services']) ?>">
                                            Servicii
                                        </a>
                                    </li>
                                    <li>
                                        <a href="<?= Url::to(['/portfolio']) ?>">
                                            Portfolio
                                        </a>
                                    </li> 
                                    <li>
                                        <a href="<?= Url::to(['/projects']) ?>">
                                            Proiecte
                                        </a>
                                    </li> 
                                </ul>
                                
                            </div>
                    </div>
                </div>
                <div class="uk-width-medium-7-10">
                    <div class="content">
                        <div class="title">
                            <?= $article->lang->Title ?>
                        </div>
                        
                        <div class="description-content">
                           <?= $article->lang->Text ?>
                        </div>
                        
                        <div class="img-content mt20 mb20">
                            <?= ArticleImageWidget::widget([
                                'article' => $article
                            ]) ?>
                            <?= ArticleFileWidget::widget([
                                'article' => $article
                            ]) ?>
                        </div>
                        
                    </div>
                </div>
            </div>
        </div>

        <?= $this->render("../layouts/blocks/footer_block") ?>
    
    </div>

                