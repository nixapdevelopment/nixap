<?php

    use yii\helpers\Html;
    use yii\widgets\Pjax;
    use yii\widgets\ActiveForm;
    use app\assets\NixapFrontAssets;
?>


    
    
    <?php if (Yii::$app->session->getFlash('newslatterSend', false)) { ?>
        <?= Html::tag('div', Yii::t('app', 'Va-ti abonat cu succes!'), [
            'class' => 'success-response'
        ]) ?>
    <?php } else { ?>
    
        <?php $form = ActiveForm::begin([    
            'id' => 'subscribe-form',
            'options' => [
                'data-pjax' => '0',
                "method" => "post",                
            ],
            'fieldConfig' => [
                'template' => "{error}\n{input}",
                'options' => [
                    'tag' => false,
                ],                
            ],
        ]); ?>
    
            <?= $form->field($model, 'Email')->input("email", ["placeholder"=>"Email", "required"=>"required"]) ?>
                        
            <?= Html::submitButton(Yii::t("app", "Abonare"), ['class' => 'btn']) ?>

        <?php ActiveForm::end(); ?>
    
    <?php } ?>
<?php $this->registerJs("
    $('#subscribe-form').on('beforeSubmit', function(e) {
    var form = $(this);
    var formData = form.serialize();
    form.attr('disabled', true);
    $.ajax({
        url: form.attr('action'),
        type: form.attr('method'),
        data: formData,
        success: function (data) {
            form.attr('disabled', false);
            form.trigger('reset');
            form.append('<div class=\"success-response\">Abonare cu succes</div>');            
        },
        error: function () {
            //alert('Something went wrong');
        }
    });
}).on('submit', function(e){
    e.preventDefault();
});
", yii\web\View::POS_READY) ?>
    
    
