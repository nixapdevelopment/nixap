<br />
<div class="row">
    <div class="col-md-6">
        <div class="panel panel-success">
            <div class="panel-heading">
                <h4>User for last month</h4>
            </div>
            <div class="panel-body">
                <div id="chart-container"></div>
            </div>
        </div>
    </div>
    <div class="col-md-6">
        <div class="panel panel-success">
            <div class="panel-heading">
                <h4>Referring Sites</h4>
            </div>
            <div class="panel-body">
                <div id="chart-container-2"></div>
            </div>
        </div>
    </div>
</div>