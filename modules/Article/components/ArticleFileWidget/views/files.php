<?php

    use yii\widgets\ActiveForm;
    use app\modules\Article\components\ArticleFileWidget\components\DateHelper;
    use yii\widgets\ListView;
    use yii\widgets\Pjax;
    use yii\helpers\Html;
    
    $yearOptions = ['' => '-'];
    for ($year = date('Y'); $year >= 2005 ; $year--)
    {
        $yearOptions[$year] = $year;
    }
    
    $dayOptions = ['' => '-'];
    for ($day = 1; $day <= 31; $day++)
    {
        $dayOptions[$day] = $day;
    }

?>

<?php Pjax::begin([
    'id' => 'files-pjax',
]) ?>
    <div>
        <?php if ($useDateFilter) { ?>
        <?php $form = ActiveForm::begin([
            'id' => 'file-filter-form',
            'options' => [
                'data-pjax' => '',
            ],
            'method' => 'get',
            'action' => yii\helpers\Url::current([])
        ]) ?>
        <div class="row">
            <div class="col-md-2">
                <?= $form->field($searchModel, 'Year')->dropDownList($yearOptions) ?>
            </div>
            <div class="col-md-2">
                <?= $form->field($searchModel, 'Month')->dropDownList(DateHelper::monthsList(), [
                    'id' => 'month-select'
                ]) ?>
            </div>
            <div class="col-md-2">
                <?= $form->field($searchModel, 'Day')->dropDownList($dayOptions, [
                    'id' => 'day-select'
                ]) ?>
            </div>
            <div class="col-md-2">
                <div class="form-group">
                    <label class="control-label hidden-xs hidden-sm">&nbsp;</label>
                    <?= Html::submitButton(Yii::t('app', 'Apply filter'), ['class' => 'btn btn-danger btn-block']) ?>
                </div>
            </div>
        </div>
        <?php ActiveForm::end() ?>
        <?php } ?>
    </div>

    <div>
        <?= ListView::widget([
            'dataProvider' => $dataProvider,
            'itemView' => 'file_item',
            'layout' => "{items}\n<div class=\"clearfix\"></div>{pager}",
            'options' => [
                'class' => 'list-group'
            ]
        ]); ?>
    </div>
<?php Pjax::end() ?>

<?php $this->registerJs("
    $(document).on('change', 'select#month-select', function(){
        var daySelect = $('#file-filter-form select#day-select');
        if ($(this).val() == '')
        {
            daySelect.attr('disabled', 'disabled');
        }
        else
        {
            daySelect.removeAttr('disabled');
        }
    });
", yii\web\View::POS_READY) ?>