var TGridView = {
	pjaxSelector: '',
	positionSelector: '',
	init: function(pjaxSelector, positionSelector, enablePosition)
	{
		TGridView.pjaxSelector = pjaxSelector;
		TGridView.positionSelector = positionSelector;
		
		if(enablePosition)
		{
			TGridView.setSortable('position-wrap', 'level-0');
			TGridView.setSortable('level-0', 'level-1');
			TGridView.setSortable('level-1', 'level-2');

			$(TGridView.pjaxSelector).on("pjax:end", function()
			{
				TGridView.init(TGridView.pjaxSelector, TGridView.positionSelector, true);
			});

			$(TGridView.positionSelector).on('hidden.bs.modal', function (e)
			{
				$.pjax.reload({container: TGridView.pjaxSelector});
			});			
		}
		
		$(pjaxSelector).on('pjax:complete', function() {
			$(pjaxSelector + ' select[data-krajee-select2]').each(function(){
				var _elem = $(this), _settings_var = _elem.attr('data-krajee-select2'), id = _elem.attr('id'), _settings = window[_settings_var];
				$.when($('#' + id).select2(_settings)).done(initS2Loading(id, '.select2-container--krajee'));
			});
		});
	},
	
	setSortable: function(parent, child)
	{
		$('.' + parent).sortable(
		{
			items: '.' + child,
			cursor:'move',
			axis: 'y',
			opacity:0.3,
			placeholder: "e-sortable-highlight",
			start: function(e, ui)
			{
				ui.placeholder.height(ui.helper.outerHeight());
			},
			update: function (event, ui) 
			{
				var data = $(this).sortable( 'serialize' );
				var url = $(this).find('.' + child).first().attr('url');
				
				TGridView.sendPost(url, data);
			}
		});
	},
	
	sendPost: function(url, data)
	{
		$.post(url, data);
	}
};