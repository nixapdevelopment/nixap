<?php

    use app\modules\Article\components\ArticleImageWidget\ArticleImageWidget;
    use app\modules\Article\components\ArticleFileWidget\ArticleFileWidget;
    use app\models\Article\Article;
    
    $article->Viewed = $article->Viewed + 1;
    $article->save(false);
    
    $this->title = $article->lang->Title;
    
    $latestProjects = Article::getDb()->cache(function ($db) {
        return Article::find()->with(['lang', 'link'])->where(['Type' => 'Project', 'Status' => 'Active'])->orderBy('Date DESC')->limit(10)->all();
    }, 60);
    
    
?>


<div class="wrapper portfolio blog">
    
        <?= $this->render("../layouts/blocks/header_block") ?>

        <div class="uk-container-center uk-container">
            <div class="uk-grid mt30 mb30">
                <div class="uk-width-medium-3-10">
                    <div class="uk-width-1-1">
                        <div class="left-sidebar">
                            <div class="title">
                                <h4>
                                    Proiecte.
                                </h4>
                            </div>
                            <div class="blog-dropdown">
                                <a href="#">
                                    <i class="uk-icon-navicon"></i>
                                </a>
                            </div>
                             <?php if (count($latestProjects) > 0) { ?>
                            <ul>
                                <?php foreach ($latestProjects as $project) { ?>
                                    <li>
                                        <a href="<?= $project->seoLink ?>">
                                            <?= $project->lang->Title ?>
                                        </a>
                                    </li>                                           
                                <?php } ?>
                            </ul>
                            <?php } ?>


                        </div>
                    </div>
                </div>
                <div class="uk-width-medium-7-10">
                    <div class="content">
                        <div class="title">
                            <?= $article->lang->Title ?>
                        </div>
                        
                        <div class="description-content">
                           <?= $article->lang->Text ?>
                        </div>
                        
                        <div class="img-content mt20 mb20">
                            <?= ArticleImageWidget::widget([
                                'article' => $article
                            ]) ?>
                            <?= ArticleFileWidget::widget([
                                'article' => $article
                            ]) ?>
                        </div>
                        
                    </div>
                </div>
            </div>
        </div>

        <?= $this->render("../layouts/blocks/footer_block") ?>
    
    </div>

                

                