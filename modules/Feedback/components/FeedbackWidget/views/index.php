<?php

    use yii\helpers\Html;
    use yii\widgets\Pjax;
    use yii\widgets\ActiveForm;
?>


    
    
    <?php if (Yii::$app->session->getFlash('feedbackSend', false)) { ?>
        <?= Html::tag('div', Yii::t('app', 'Mesaj transmis cu succes!'), [
            'class' => 'success-response'
        ]) ?>
    <?php } else { ?>
    
        <?php $form = ActiveForm::begin([                       
            'options' => [
                'data-pjax' => '0',
                'enctype' => 'multipart/form-data',
                "method" => "post",                
            ],
            'fieldConfig' => [
                'template' => "<label>\n{input}\n{error}\n</label>",
                'options' => [
                            'tag' => false,
                        ],                
            ],
        ]); ?>
    
            <label>
                <div class="title">
                    <h4>
                        <?= Yii::t("app", "Start Proiect") ?>
                    </h4>
                </div>
            </label>

            <?= $form->field($model, 'Name')->textInput(["placeholder"=>"Nume/Prenume", "required"=>"required"]) ?>

            <?= $form->field($model, 'Phone')->textInput(["placeholder"=>"Numar Telefon", "required"=>"required"]) ?>

            <?= $form->field($model, 'Email')->textInput(["placeholder"=>"Email", "required"=>"required"]) ?>

            <?= $form->field($model, 'Message')->textarea(["placeholder"=>"Detalii despre proiectul D-stră", "required"=>"required"]) ?>

            <?= Html::hiddenInput('widgetID', $widgetID); ?>

            <label class="upload uk-clearfix" for="upload5">
                <i class="uk-icon-cloud-upload"></i>
                <p>Încarcă fișier</p>
            </label>

            <input id="upload5" type="file" name="File[]" multiple class="custom-file-input">
            
                        
            <?= Html::submitButton(Yii::t("app", "Trimite"), ['class' => 'btn']) ?>

        <?php ActiveForm::end(); ?>
    
    <?php } ?>
    
    
