<?php

    use yii\helpers\Html;
    use yii\helpers\Url;
    use yii\widgets\Pjax;

?>

<?php Pjax::begin([
    'id' => 'gallery-image-form-pjax',
    'enablePushState' => false,
]) ?>

    <?= Html::beginForm(Url::to(['/admin/gallery/gallery/upload-images']), 'post', [
        'id' => 'gallery-image-form',
        'data-pjax' => ''
    ]) ?>
        <div class="row">
            <div class="col-md-5">
                <div class="form-group">
                    <?= Html::fileInput('Images[]', '', [
                        'class' => 'form-control',
                        'required' => 'required',
                        'multiple' => true
                    ]) ?>
                    <?= Html::hiddenInput('GalleryID', $model->ID) ?>
                </div>
            </div>
            <div class="col-md-4">
                <div class="form-group">
                    <?= Html::submitButton(Yii::t('app', 'Submit'), [
                        'class' => 'btn btn-success',
                    ]) ?>
                </div>
            </div>
        </div>
    <?= Html::endForm() ?>

<?php Pjax::end(); ?>

<?php $this->registerJs('
    $(document).on("pjax:success", "#gallery-image-form-pjax",  function(event){
        $.pjax.reload({container: "#gallery-item-list"});
    });
') ?>