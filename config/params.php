<?php

return [
    'adminEmail' => 'colesnic89@gmail.com',
    'siteLanguages' => ['ro', 'ru'],
    'googleAnalitics' => [
        'siteUACode' => 'UA-35194014-1',
        'clientId' => '147819421403-9pgqg6u5v8qakilc3lpgeve1fuabsttm.apps.googleusercontent.com',
    ]
];
