<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\Category\Category */

$this->title = 'Create Menu';
$this->params['breadcrumbs'][] = ['label' => 'Menus', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="category-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
        'langsModels' => $langsModels
    ]) ?>

</div>
