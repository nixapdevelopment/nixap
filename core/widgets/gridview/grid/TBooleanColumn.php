<?php
namespace template\widgets\gridview\grid;

use Yii;
use Closure;
use kartik\grid\BooleanColumn;
/**
 * A BooleanColumn to convert true/false values as user friendly indicators with an automated drop down filter for the
 * Grid widget [[\kartik\widgets\GridView]]
 *
 * @author Kartik Visweswaran <kartikv2@gmail.com>
 * @since 1.0
 */
class TBooleanColumn extends BooleanColumn
{
    /**
     * @var string label for the true value. Defaults to `Active`.
     */
    public $trueValue = 'Active';

    /**
     * @var string label for the false value. Defaults to `Inactive`.
     */
    public $falseValue = 'Disabled';

	
    public function init()
    {
        parent::init();
    }

    /**
     * @inheritdoc
     */
    public function getDataCellValue($model, $key, $index)
    {
        $value = $model->{$this->attribute};
		
		switch ($value)
		{
			case $this->trueValue:
				return $this->trueIcon;
			break;
		
			case $this->falseValue:
				return $this->falseIcon;
			break;
		
			default :
				return $this->showNullAsFalse ? $this->falseIcon : $value;
			break;
		}
    }
}
