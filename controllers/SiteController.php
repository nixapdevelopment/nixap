<?php

namespace app\controllers;

use Yii;
use app\controllers\FrontController;
use app\models\Article\Article;
use app\models\Link\Link;



class SiteController extends FrontController
{
    
    public $layout = 'frontend';

    
    public function actionIndex($route)
    {
        return $this->resolveRoute($route);
    }
    
    public function resolveRoute($route)
    {
        $segments = explode('/', $route);
        $segments = array_filter($segments);
        
        $current_segment = end($segments);
        
        if ($current_segment)
        {
            $item = Link::find()->with(['article', 'article.lang'])->where(['Link' => $current_segment])->one();
            
            if (!empty($item->article) && $item->article != NULL)
            {
                $article = $item->article;
                
                if (empty($article->Template))
                {
                    $view = strtolower($article->Type);
                }
                else
                {
                    $view =  strtolower($article->Template);
                }
                
                return $this->render($view, [
                    'article' => $article
                ]);
            }
        }
        else
        {
            $article = Article::find()->with(['lang'])->where(['ID' => 8])->one();
            
            $this->layout = "home-layout";
            
            return $this->render('home', [
                'article' => $article
            ]);
        }
        
        return $this->actionError();
    }

    public function actionError()
    {
        return $this->render('error', [
            'message' => 'Page not found'
        ]);
    }

}