<?php

    use app\modules\Article\components\ArticleImageWidget\ArticleImageWidget;
    use app\modules\Article\components\ArticleFileWidget\ArticleFileWidget;
    use app\models\Article\Article;
    use app\modules\Newslatter\components\NewslatterWidget\NewslatterWidget;
    
    $article->Viewed = $article->Viewed + 1;
    $article->save(false);
    
    $this->title = $article->lang->Title;
    
    $latestBlogs = Article::getDb()->cache(function ($db) {
        return Article::find()->with(['lang', 'link'])->where(['Type' => 'Blog', 'Status' => 'Active'])->orderBy('Date DESC')->limit(10)->all();
    }, 60);
    
?>

<div class="wrapper portfolio blog">
    
        <?= $this->render("../layouts/blocks/header_block") ?>

        <div class="uk-container-center uk-container">
            <div class="uk-grid mt30 mb30">
                <div class="uk-width-medium-3-10">
                    <div class="uk-width-1-1">
                        <div class="left-sidebar">
                            <div class="title">
                                <h4>
                                    Blog.
                                </h4>
                            </div>
                            <div class="blog-dropdown">
                                <a href="#">
                                    <i class="uk-icon-navicon"></i>
                                </a>
                            </div>
                             <?php if (count($latestBlogs) > 0) { ?>
                            <ul>
                                <?php foreach ($latestBlogs as $blog) { ?>
                                    <li>
                                        <a href="<?= $blog->seoLink ?>">
                                            <?= $blog->lang->Title ?>
                                        </a>
                                    </li>                                           
                                <?php } ?>
                            </ul>
                            <?php } ?>


                        </div>
                        <div class="left-sidebar sidebar2 mt20">
                            <div class="title">
                                <h4>
                                    <?= Yii::t("app", "Aboneazăt-te la Newslatter.") ?>
                                </h4>
                            </div>

                            <?= NewslatterWidget::widget() ?>

                        </div>
                    </div>
                </div>
                <div class="uk-width-medium-7-10">
                    <div class="content">
                        <div class="title">
                            <?= $article->lang->Title ?>
                        </div>
                        
                        <div class="description-content">
                           <?= $article->lang->Text ?>
                        </div>
                        
                        <div class="img-content mt20 mb20">
                            <?= ArticleImageWidget::widget([
                                'article' => $article
                            ]) ?>
                            <?= ArticleFileWidget::widget([
                                'article' => $article
                            ]) ?>
                        </div>
                        
                    </div>
                </div>
            </div>
        </div>

        <?= $this->render("../layouts/blocks/footer_block") ?>
    
    </div>

                