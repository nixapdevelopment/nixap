<?php

    use yii\web\JsExpression;
    use pigolab\locationpicker\CoordinatesPicker;

?>
<br />
<?=  CoordinatesPicker::widget([
    'name' => 'text',
    'key' => 'AIzaSyArs0gE3iwHLdxXvppAJdo1JA6fXikOiGE',
    'options' => [
        'style' => 'width: 100%; height: 500px',
        'class' => 'location-picker'
    ] ,
    'clientOptions' => [
        'zoom' => 15,
        'location' => [
            'latitude'  => (float)$model->Lat,
            'longitude' => (float)$model->Long,
        ],
        'radius' => 0,
        'addressFormat' => 'street_number',
        'inputBinding' => [
            'latitudeInput' => new JsExpression("$('#location-lat')"),
            'longitudeInput' => new JsExpression("$('#location-long')"),
        ]
    ]        
]); ?>
<br />
<div class="row">
    <div class="col-md-6">
        <?= $form->field($model, 'Long')->textInput([
            'id' => 'location-long'
        ]) ?>
    </div>
    <div class="col-md-6">
        <?= $form->field($model, 'Lat')->textInput([
            'id' => 'location-lat'
        ]) ?>
    </div>
</div>