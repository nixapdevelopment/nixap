<?php

use yii\bootstrap\Html;
use app\assets\NixapFrontAssets;
use app\modules\Feedback\components\FeedbackWidget\FeedbackWidget;
use app\modules\Article\components\ArticleImageWidget\ArticleImageWidget;
use app\modules\Article\components\ArticleFileWidget\ArticleFileWidget;
use yii\widgets\ListView;
use yii\widgets\Pjax;
use app\models\Article\Article;
use yii\data\ActiveDataProvider;
use app\modules\Settings\Settings;
    
$bundle = NixapFrontAssets::register($this);

$this->title = $article->lang->Title;

$query = Article::getDb()->cache(function ($db) {
    return Article::find()->with(['lang', 'link'])->where(['Type' => 'Project', 'Status' => 'Active'])->orderBy('Date DESC');
}, 60);

$lastProject = Article::getDb()->cache(function ($db) {
    return Article::find()->with(['lang', 'link'])->where(['Type' => 'Project', 'Status' => 'Active'])->orderBy('Date DESC')->limit(1)->one();
}, 60);

$dataProvider = new ActiveDataProvider([
    'query' => $query,
    'pagination' => [
        'pageSize' => 9999
    ],
]);
    
?>

<div class="wrapper services-page proiect">
    
     <?= $this->render("../layouts/blocks/header_block") ?>        
        
        <div class="uk-clearfix proiect-content" id="progress-bar">
            <div class="uk-container-center uk-container">
                <div>
                    <div class="uk-grid">
                        <div class="uk-width-1-1">
                            <div class="title">
                                <p>
                                    <?= $article->lang->Text ?>
                                </p>
                            </div>
                        </div>
                        <div class="uk-width-1-3">
                            <div class="progress-bar">
                                <div id="box" class="box box1">

                                </div>
                            <span>
                                ani pe piață
                            </span>
                            </div>
                        </div>
                        <div class="uk-width-1-3">
                            <div class="progress-bar">
                                <div id="box2" class="box box2">

                                </div>
                                <span>
                                    de clienți saptămânal
                                </span>
                            </div>
                        </div>
                        <div class="uk-width-1-3">
                            <div class="progress-bar">
                                <div id="box3" class="box box3">

                                </div>
                                <span>
                                    mii de produse
                                </span>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="uk-clearfix proiect-content proiect-content2">
            <div class="uk-container-center uk-container">
                <div class="uk-grid">
                    <div class="uk-width-1-1">
                        <div class="title">
                            <h3>
                               Sarcina
                            </h3>
                            <p>
                                Morbi in sem quis dui placerat ornare. Pellentesque odio nisi, euismod in,
                                pharetra a, ultricies in, diam. Sed arcu. Cras consequat.
                            </p>
                        </div>
                        <div id="container1" class='twentytwenty-container'>
                            <?= Html::img($bundle->baseUrl . "/images/Layer-1-1.jpg") ?>
                            <?= Html::img($bundle->baseUrl . "/images/Layer-1-3.jpg") ?>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="proiect-content3">
            <div class="uk-container-center uk-container">
                <div class="btn-site uk-text-center">                    
                    <a class="btn" href="<?= $lastProject->seoLink ?>">
                       <?= $lastProject->lang->Title ?>
                    </a>
                </div>
            </div>
        </div>
        <div class="uk-clearfix other-proiects">
            <div class="uk-container-center uk-container">
                <div class="uk-grid">
                    <div class="uk-width-1-1">
                        <div class="title">
                            <h4>
                                Alte proiecte
                            </h4>
                        </div>
                    </div>
                    
                    <?php Pjax::begin(['options' => ['tag'=>false]]); ?>
                        <?= ListView::widget([
                            'dataProvider' => $dataProvider,
                            'itemView' => function ($model, $key, $index, $widget){
                                if ($index == 0)
                                    {
                                        return false;
                                    }
                                    return $this->render('project_list_item', [
                                        'model' => $model
                                    ]);
                                },
                            'layout' => '{items}',
                            'options' => ['tag'=>false],
                            'itemOptions' => ['tag'=>false],

                        ]); ?>
                    <?php Pjax::end(); ?>                     
                  
                    <div class="uk-width-1-1">
                        <div class="form-box">
                            <div class="form">
                                
                                <?= FeedbackWidget::widget() ?>
                                
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        
    <?= $this->render("../layouts/blocks/footer_block") ?>
    
</div>
