<?php

    use app\modules\Feedback\components\FeedbackWidget\FeedbackWidget;
    use app\modules\Article\components\ArticleImageWidget\ArticleImageWidget;
    use app\modules\Article\components\ArticleFileWidget\ArticleFileWidget;
    use app\modules\Settings\Settings;
    
    $this->title = $article->lang->Title;
    
?>

<div class="wrapper contacts">
    
    <?= $this->render("../layouts/blocks/header_block") ?>

        <div class="uk-clearfix our-contacts">
            <div class="uk-container-center uk-container">
                <div class="uk-grid">
                    <div class="uk-width-1-2">
                        <div class="adress-box">
                            <div class="title">
                                <h4>
                                    Ne găsiți
                                </h4>
                            </div>
                            <div class="uk-grid">
                                <div class="uk-width-1-2">
                                    <div class="box1 box">
                                        <h5>
                                            Office 1
                                        </h5>
                                        <ul>
                                            <li>
                                                <a href="#">
                                                    <i class="uk-icon-envelope"></i>
                                                        <span>
                                                            <?= Settings::getByName('email') ?>
                                                        </span>
                                                </a>
                                            </li>
                                            <li>
                                                <a href="tel:+373<?= Settings::getByName('phone') ?>">
                                                    <i class="uk-icon-phone"></i>
                                                    +373-<?= Settings::getByName('phone') ?>
                                                </a>
                                            </li>
                                            <li>
                                                <a href="#">
                                                    <i class="uk-icon-map-marker"></i>
                                                    Moldova, Chișinău
                                                </a>
                                            </li>
                                        </ul>
                                    </div>
                                </div>
                                <div class="uk-width-1-2">
                                    <div class="box2 box">
                                        <h5>
                                            Office 2
                                        </h5>
                                        <ul>
                                            <li>
                                                <a href="#">
                                                    <i class="uk-icon-envelope"></i>
                                                        <span>
                                                            <?= Settings::getByName('email2') ?>
                                                        </span>
                                                </a>
                                            </li>
                                            <li>
                                                <a href="tel:+37360999913">
                                                    <i class="uk-icon-phone"></i>
                                                    +373-60-9999-13
                                                </a>
                                            </li>
                                            <li>
                                                <a href="#">
                                                    <i class="uk-icon-map-marker"></i>
                                                    Moldova, Chișinău
                                                </a>
                                            </li>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="uk-width-1-2">
                                                
                        <div class="form-box">
                            <div class="form">
                                
                                <?= FeedbackWidget::widget() ?>
                                
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
   

        <?= $this->render("../layouts/blocks/footer_block") ?>

    </div>