<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Category\Category */

$this->title = 'Update Category: ' . reset($model->categoryLangs)->Title;
$this->params['breadcrumbs'][] = ['label' => 'Categories', 'url' => ['index']];
$this->params['breadcrumbs'][] = reset($model->categoryLangs)->Title;
?>
<div class="category-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
        'langsModels' => $langsModels
    ]) ?>

</div>
