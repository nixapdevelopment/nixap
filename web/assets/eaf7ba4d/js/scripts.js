
$(document).ready(function(){


    $('.toggle-bottom1').on("click", function(e){
        e.preventDefault();
        $(".first-list").slideToggle();
    });

    $('.toggle-bottom2').on("click", function(e){
        e.preventDefault();
        $(".secound-list").slideToggle();
    });

    $('.toggle-bottom3').on("click", function(e){
        e.preventDefault();
        $(".thirdth-list").slideToggle();
    });

    $('.toggle-bottom4').on("click", function(e){
        e.preventDefault();
        $(".fourth-list").slideToggle();
    });

    $('.toggle-bottom5').on("click", function(e){
        e.preventDefault();
        $(".fiveth-list").slideToggle();
    });

    $('.toggle-bottom6').on("click", function(e){
        e.preventDefault();
        $(".sixth-list").slideToggle();
    });

    $('.blog-dropdown > a').on("click", function(e){
        e.preventDefault();
        $(this).parent().parent().find("ul").slideToggle();
    });

    if (window.innerWidth > 920) {
        $.UIkit.sticky('.sidebar2', {top: 10});
    }

    if (window.innerWidth > 920) {
        $.UIkit.sticky('.sidebar1', {top: 10});
    }

    if (window.innerWidth > 920) {
        $.UIkit.sticky('.sidebar3', {top: 10});
    }

    if (window.innerWidth > 920) {
        $.UIkit.sticky('.sidebar4', {top: 10});
    }



    /* POPUP MODAL */

    $('.popup-modal').magnificPopup({
        type: 'inline',
        preloader: false,
        modal: true
    });
     $(document).on('click', '.popup-modal-dismiss', function (e) {
        e.preventDefault();
        $.magnificPopup.close();
    });


    /* POPUP VIDEO */

    $('.popup-video').magnificPopup({
        disableOn: 700,
        type: 'iframe',
        mainClass: 'mfp-fade',
        removalDelay: 160,
        preloader: false,

        fixedContentPos: false
    });


    /* OWL SLIDER */

    $('.slider').owlCarousel({
        loop:true,
        margin:10,
        dots: true,
        autoplay: true,
        autoplayTimeout:10000,
        responsive:{
            0:{
                items:1
            }
        }
    });
//    $(".owl-dot > span").after("<img src='/images/dots.png'/>");
//    $(".owl-dot > span").before("<img src='/images/dots.png'/>");


    /* OWL SLIDER 2 */

    $('.slider2').owlCarousel({
        loop:true,
        margin:10,
        nav:true,
        dots: false,
        navText: ["<i class='uk-icon-chevron-left'></i>","<i class='uk-icon-chevron-right'></i>"],
        responsive:{
            0:{
                items:1
            }
        }
    });


    /* BEFORE AND AFTER SLIDER */

    $(window).load(function() {
        $("#container1").twentytwenty();
    });




});












