<?php

namespace app\models\Newslatter;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\Newslatter\Newslatter;

/**
 * SearchNewslatter represents the model behind the search form about `app\models\Newslatter\Newslatter`.
 */
class SearchNewslatter extends Newslatter
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['ID'], 'integer'],
            [['Email', 'Date'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Newslatter::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'ID' => $this->ID,
            'Date' => $this->Date,
        ]);

        $query->andFilterWhere(['like', 'Email', $this->Email]);

        return $dataProvider;
    }
}
