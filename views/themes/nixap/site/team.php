<?php

use yii\bootstrap\Html;
use yii\helpers\Url;
use app\modules\Article\components\ArticleImageWidget\ArticleImageWidget;
use app\modules\Article\components\ArticleFileWidget\ArticleFileWidget;
use app\models\SliderItem\SliderItem;
use yii\caching\TagDependency;
use app\assets\NixapFrontAssets;

$bundle = NixapFrontAssets::register($this);

$this->title = $article->lang->Title;

$slides = SliderItem::getDb()->cache(function ($db) {
    return SliderItem::find()->with('lang')->where(['SliderID' => 3])->all();
}, 30, new TagDependency(['tags' => 'slider']));
    
?>


<div class="wrapper our-team">
    
    <?= $this->render("../layouts/blocks/header_block") ?>
           
    <div class="uk-clearfix our-slider2">                

        <?php if (count($slides) > 0) { ?>
        <div class="my-slider">
            <?php foreach ($slides as $slide) { ?>
                <div class="my-Slides">
                    <?= Html::img($slide->imageUrl) ?>
                    <div class="description">
                        <h3>
                            <?= $slide->lang->Title ?>
                        </h3>
                        <?= $slide->lang->Text ?>
                    </div>
                </div>
            <?php } ?>

            <div class="slider-nav">
                <button class="prev">
                    <?= Html::img($bundle->baseUrl . '/images/chevron-left.png', ["onclick"=>"plusDivs(-1)"]) ?>
                </button>
                <button class="next">
                    <?= Html::img($bundle->baseUrl . '/images/chevron-right.png', ["onclick"=>"plusDivs(+1)"]) ?>
                </button>
            </div>
            <div class="dots">

            <?php $i=1; foreach ($slides as $slide) { ?>
                <div class="dot">
                    <?= Html::img($slide->imageUrl, ["class"=>"demo", "onclick"=>"currentDiv(".$i.")", "title"=>$slide->lang->Title]) ?>
                </div>
            <?php $i++; } ?>

            </div>
        </div>
        <?php } ?>

    </div>
        
    <?= $this->render("../layouts/blocks/footer_block") ?>

</div>
