<?php

    use app\core\widgets\TinyMce;

?>

<br />

<?= $form->field($lmodel, "[$key]Title")->textInput(['maxlength' => true]) ?>

<?= $form->field($lmodel, "[$key]Text")->widget(TinyMce::className(), [
    'language' => Yii::$app->language
]) ?>