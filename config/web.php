<?php

$params = require(__DIR__ . '/params.php');

$config = [
    'id' => 'basic',
    'language' => 'ro',
    'basePath' => dirname(__DIR__),
    'bootstrap' => ['log'],
    'components' => [
        'assetManager' => [
            'bundles' => [
                'dosamigos\google\maps\MapAsset' => [
                    'options' => [
                        'key' => 'AIzaSyC_XXNZQK5LYRLij0oQNGPpxD4JVOVcJhU',
                        'language' => Yii::$app->language,
                        'version' => '3.1.18'
                    ]
                ]
            ],
            'forceCopy' => true
        ],
        'request' => [
            // !!! insert a secret key in the following (if it is empty) - this is required by cookie validation
            'cookieValidationKey' => 'aH-Duaufn8iaqyzr0F8yW0tlPKdhhLDC',
            'baseUrl' => ''
        ],
        'cache' => [
            'class' => 'yii\caching\FileCache',
        ],
        'user' => [
            'identityClass' => 'app\models\User\User',
            'enableAutoLogin' => true,
        ],
        'errorHandler' => [
            'errorAction' => 'site/error',
        ],
        'mail' => [
            'class' => 'yii\swiftmailer\Mailer',
            'viewPath' => '@app/mail',
            'useFileTransport' => false,
//            'transport' => [
//                'class' => 'Swift_SmtpTransport',
//                'host' => 'smtp.gmail.com',
//                'username' => 'test@gmail.com',
//                'password' => '',
//                'port' => '587',
//                'encryption' => 'tls',
//            ],
        ],
        'log' => [
            'traceLevel' => YII_DEBUG ? 3 : 0,
            'targets' => [
                [
                    'class' => 'yii\log\FileTarget',
                    'levels' => ['error', 'warning'],
                ],
            ],
        ],
        'db' => require(__DIR__ . '/db.php'),
        'cache' => [
            'class' => 'yii\caching\FileCache',
        ],
        'urlManager' => [
            'class' => 'app\core\CoreUrlManager',
            'languages' => $params['siteLanguages'],
            'enablePrettyUrl' => true,
            'showScriptName' => false,
            'enableDefaultLanguageUrlCode' => true,
            'enableLanguageDetection' => false,
            'rules' => [
                'admin' => 'admin/dashboard',
                'admin/<module>/<controller>/<action>' => 'admin/<module>/<controller>/<action>',
                'admin/<module>/<controller>/' => 'admin/<module>/<controller>',
                'admin/<module>/' => 'admin/<module>',
                '<route:(.*)>' => 'site/index',
            ]
        ],
        'view' => [
            'theme' => [
                'basePath' => '@app/views/themes/nixap',
                'baseUrl' => '@web/themes/nixap',
                'pathMap' => [
                    '@app/views' => '@app/views/themes/nixap',
                ],
            ],
        ],
        'i18n' => [
            'translations' => [
                'app*' => [
                    'class' => 'yii\i18n\PhpMessageSource',
                    'basePath' => '@app/messages',
                    'fileMap' => [
                        'app'       => 'app.php',
                    ],
                ],
            ],
        ],
    ],
    'modules' => [
        'admin' => [
            'class' => 'app\modules\Admin\Admin',
            'modules' => [
                'user' => [
                    'class' => 'app\modules\User\User',
                    'defaultRoute' => 'user/index',
                ],
                'dashboard' => [
                    'class' => 'app\modules\Dashboard\Dashboard',
                ],
                'article' => [
                    'class' => 'app\modules\Article\Article',
                    'defaultRoute' => 'article/index',
                ],
                'menu' => [
                    'class' => 'app\modules\Menu\Menu',
                    'defaultRoute' => 'menu/index',
                ],
                'slider' => [
                    'class' => 'app\modules\Slider\Slider',
                ],
                'settings' => [
                    'class' => 'app\modules\Settings\Settings',
                    'defaultRoute' => 'default/settings',
                ],
                'feedback' => [
                    'class' => 'app\modules\Feedback\Feedback',
                    'defaultRoute' => 'feedback/index',
                ],                
                'newslatter' => [
                    'class' => 'app\modules\Newslatter\Newslatter',
                    'defaultRoute' => 'newslatter/index',
                ],
                'gallery' => [
                    'class' => 'app\modules\Gallery\Gallery',
                    'defaultRoute' => 'gallery/index',
                ],
                'project' => [
                    'class' => 'app\modules\Project\Project'
                ],
            ]
        ],
        
        // system modules
        'gridview' =>  [
            'class' => '\kartik\grid\Module',
            // enter optional module parameters below - only if you need to  
            // use your own export download action or custom translation 
            // message source
            // 'downloadAction' => 'gridview/export/download',
            // 'i18n' => []
        ]
    ],
    'params' => $params,
];

if (YII_ENV_DEV) {
    // configuration adjustments for 'dev' environment
    $config['bootstrap'][] = 'debug';
    $config['modules']['debug'] = [
        'class' => 'yii\debug\Module',
        // uncomment the following to add your IP if you are not connecting from localhost.
        //'allowedIPs' => ['127.0.0.1', '::1'],
    ];

    $config['bootstrap'][] = 'gii';
    $config['modules']['gii'] = [
        'class' => 'yii\gii\Module',
        // uncomment the following to add your IP if you are not connecting from localhost.
        //'allowedIPs' => ['127.0.0.1', '::1'],
    ];
}

return $config;
