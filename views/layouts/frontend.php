<?php

/* @var $this \yii\web\View */
/* @var $content string */

use yii\bootstrap\Html;
use app\assets\FrontAssets;
use yii\helpers\Url;
use pceuropa\languageSelection\LanguageSelection;


FrontAssets::register($this);
?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">
<head>
    <meta charset="<?= Yii::$app->charset ?>">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <?= Html::csrfMetaTags() ?>
    <title><?= Html::encode($this->title) ?></title>
    <?php $this->head() ?>
</head>
<body>
<?php $this->beginBody() ?>

<header class="header">
    <div class="top-bar">
        <div class="container">
            <div class="pull-right">
                <?= LanguageSelection::widget([
                    'language' => Yii::$app->params['siteLanguages'],
                    'languageParam' => 'language',
                    'container' => 'div',
                    'classContainer' =>  'dropdown-toggle'
                ]) ?>
            </div>
        </div>
    </div>
    <div class="header-content">
        <div class="container">
            <div class="row">
                <div class="col-md-1">
                    <a href="<?= Url::toRoute('/') ?>" class="logotype">
                        <?= Html::img('@web/images/logo.png') ?>
                    </a>
                </div>
                <div class="col-md-6">
                    <nav class="navigation">
                        <ul>
                            <li class="active">
                                <a href="<?= Url::toRoute('/') ?>">Home</a>
                            </li>
                            <li><a href="#">Despre noi</a></li>
                            <li><a href="#">Noutăți</a></li>
                            <li><a href="#">Destinații</a></li>
                            <li><a href="#">Contacte</a></li>
                        </ul>
                    </nav>
                </div>
                <div class="col-md-5">
                    <div class="row">
                        <div class="col-md-6">
                            <a href="#" class="header-contact">
                                <?= Html::img('@web/images/icon_phone.png') ?>
                                <small>Telefon:</small>
                                <strong>+373 22 123-456</strong>
                            </a>
                        </div>
                        <div class="col-md-6">
                            <a href="#" class="header-contact">
                                <?= Html::img('@web/images/icon_map.png') ?>
                                <small>Adresa noastră:</small>
                                <strong>Ion Creangă 77/1</strong>
                            </a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</header>

<?= $content ?>
    
    <footer style="height: 100px; background-color: #ddd;">
    <div class="container">
        
    </div>
</footer>

<?php $this->endBody() ?>
    
<?php if (!empty(Yii::$app->params['googleAnalitics']['siteUACode'])) { ?>
<?php $this->registerJs("
    (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
    (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
    m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
    })(window,document,'script','https://www.google-analytics.com/analytics.js','ga');
    ga('create', '" . Yii::$app->params['googleAnalitics']['siteUACode'] . "', 'auto');
    ga('send', 'pageview');
", yii\web\View::POS_END); ?>
<?php } ?>
    
</body>
</html>
<?php $this->endPage() ?>
