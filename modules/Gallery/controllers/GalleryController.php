<?php

namespace app\modules\Gallery\controllers;

use Yii;
use app\models\Gallery\Gallery;
use app\models\GalleryLang\GalleryLang;
use app\models\Gallery\GallerySearch;
use app\controllers\BackendController;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\base\Model;
use app\models\GalleryItem\GalleryItem;

/**
 * GalleryController implements the CRUD actions for Gallery model.
 */
class GalleryController extends BackendController
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all Gallery models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new GallerySearch();
        $dataProvider = $searchModel->search();

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Creates a new Gallery model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new Gallery();
        $modelLangs = [];
        foreach (Yii::$app->params['siteLanguages'] as $key => $lang)
        {
            $modelLangs[$key] = new GalleryLang([
                'LangID' => $lang
            ]);
        }
        
        if (Yii::$app->request->isPost)
        {
            if ($model->load(Yii::$app->request->post()) && $model->validate() && Model::loadMultiple($modelLangs, Yii::$app->request->post()) && Model::validateMultiple($modelLangs))
            {
                $model->save();

                foreach ($modelLangs as $ml)
                {
                    $ml->GalleryID = $model->ID;
                    $ml->save();
                }
                
                Yii::$app->session->setFlash('success', 'Gallery was saved');
                
                if (Yii::$app->request->post('Redirect') == 'list')
                {
                    return $this->redirect(['index']);
                }

                return $this->redirect(['update', 'id' => $model->ID]);
            }
        }
        
        return $this->render('create', [
            'model' => $model,
            'modelLangs' => $modelLangs,
        ]);
    }

    /**
     * Updates an existing Gallery model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        $modelLangs = [];
        foreach (Yii::$app->params['siteLanguages'] as $key => $lang)
        {
            $modelLangs[$key] = isset($model->langs[$lang]) ? $model->langs[$lang] : new GalleryLang([
                'LangID' => $lang,
            ]);
        }
        
        if (Yii::$app->request->isPost)
        {
            if ($model->load(Yii::$app->request->post()) && $model->validate() && Model::loadMultiple($modelLangs, Yii::$app->request->post()) && Model::validateMultiple($modelLangs))
            {
                $model->save();

                foreach ($modelLangs as $ml)
                {
                    $ml->GalleryID = $model->ID;
                    $ml->save();
                }
                
                Yii::$app->session->setFlash('success', 'Gallery was saved');
                
                if (Yii::$app->request->post('Redirect') == 'list')
                {
                    return $this->redirect(['index']);
                }

                return $this->redirect(['update', 'id' => $model->ID]);
            }
        }
        
        return $this->render('create', [
            'model' => $model,
            'modelLangs' => $modelLangs,
        ]);
    }

    /**
     * Deletes an existing Gallery model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the Gallery model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Gallery the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Gallery::find()->with('langs')->where(['ID' => $id])->one()) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
    
    public function actionUploadImages()
    {
        $galleryID = (int)Yii::$app->request->post('GalleryID');
        
        $model = Gallery::findOne($galleryID);
        
        $images = \yii\web\UploadedFile::getInstancesByName('Images');
        
        $maxPosition = GalleryItem::find()->where(['GalleryID' => $galleryID])->max('Position');
        $newPosition = $maxPosition == NULL ? 0 : $maxPosition ++;

        foreach ($images as $imageIns)
        {
            $image = md5(microtime(true)) . '.' . $imageIns->extension;
            $imagePath = Yii::getAlias('@webroot/uploads/gallery/' . $image);
            
            if ($imageIns->saveAs($imagePath))
            {
                $newPosition++;
                \yii\imagine\Image::thumbnail($imagePath, 1200, 900)->save($imagePath);
                
                $thumb = 'thumb_' . $image;
                $thumbPath = Yii::getAlias('@webroot/uploads/gallery/' . $thumb);
                \yii\imagine\Image::thumbnail($imagePath, 300, 300)->save($thumbPath);
                
                $galleryImage = new GalleryItem();
                $galleryImage->GalleryID = $galleryID;
                $galleryImage->Thumb = $thumb;
                $galleryImage->Value = $image;
                $galleryImage->Type  = 'Image';
                $galleryImage->Position  = $newPosition;
                $galleryImage->save();
            }
        }
        
        return $this->renderAjax('upload-images-form', [
            'model' => $model
        ]);
    }
    
    public function actionSortImages()
    {
        $ids = (array)Yii::$app->request->post('ids');
        
        foreach ($ids as $position => $id)
        {
            GalleryItem::updateAll(['Position' => $position], ['ID' => $id]);
        }
    }
    
    public function actionDeleteImage()
    {
        $id = (int)Yii::$app->request->post('id');
        
        GalleryItem::deleteAll(['ID' => $id]);
    }
    
}
