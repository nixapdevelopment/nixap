<?php

use yii\helpers\Html;
use yii\helpers\Url;
use app\assets\NixapFrontAssets;
use app\modules\Settings\Settings;
use app\models\Article\Article;

$bundle = NixapFrontAssets::register($this);

$countBlogs = Article::getDb()->cache(function ($db) {
    return Article::find()->with(['lang', 'link'])->where(['Type' => 'Blog', 'Status' => 'Active'])->count();
}, 60);
    

?>

<div class="uk-clearfix footer">
    <div class="uk-container-center uk-container">
        <div class="link-section uk-clearfix">
            <a href="<?= Url::to(['/despre-noi']) ?>">
                Despre noi
            </a>
            <a href="<?= Url::to(['/portfolio']) ?>">
                Portofoliu
            </a>
            <a href="<?= Url::to(['/filosofie']) ?>">
                Filosofie
            </a>
            <a href="<?= Url::to(['/blog']) ?>">
                <span>
                    <?= $countBlogs ?>
                </span>
                <?= Yii::t("app", "Blog") ?>
            </a>
        </div>
    </div>
</div>